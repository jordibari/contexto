# Lupa Electoral
* Visitar la ruta /admin y loguearse.

### Carga de datos
* La carga de datos de la lupa electoral debe realizarse en el siguiente orden:
* ![Imagen 1](/readme_images/imagen_1.png)
* Mediante la pestaña Import de los modelos.
* ![Import](/readme_images/pestana_import.png)

#### 1) Division Territorial y Administrativa
* Estos datos están precargados en la aplicación.

#### 2) Pactos
* Los campos deben ser exactamente estos, pero el orden no importa.
* ![Imagen 1](/readme_images/modelo_carga_pacto_partido_cadidato.png)

#### 3) Partidos(Ver modelo plantilla pactos)

#### 4) Candidatos(Ver modelo plantilla pactos)
* ![Imagen 2](/readme_images/importacion_candidatos.png)

#### 5) Aportes / Aportantes
* Los Aportantes se cargan junto a los aportes, són la única referencia q no debe estar cargada previamente.
* Los campos deben ser exactamente estos, pero el orden no importa.
* ![Imagen 2](/readme_images/imagen_2.png)

#### 6) Denuncias
* Los campos deben ser exactamente estos, pero el orden no importa.
* ![Imagen 3](/readme_images/imagen_3.png)

#### 7) Ficheros de datos
* Los ficheros de datos para descarga deben cargarse en la sección ficheros del administrador
* ![Ficheros](/readme_images/lista_ficheros.png)

#### 8) Lugares de propaganda Electoral
* El fichero para la carga de estos datos se encuentra en la carpeta de seeds.

### Tratamiento de errores
* Durante la carga de los datos pueden suceder errores que serán informados en el listado de objetos del modelo Error del administrador.
* ![Errores](/readme_images/errores_de_carga.png)
* Si se desea incorporar los datos rechazados, se puede tomar la información de los registros de errores para corregir las filas del fichero original.

### Borrado masivo de datos
* Se puede dar el caso que queramos eliminar todos los datos de una tabla, para ello debemos ir a la pestaña Truncate del administrador del modelo del cual queremos borrar los datos:
![Truncado](/readme_images/truncate.png)
* Marcar la casilla y aceptar:
* ![Truncado](/readme_images/truncate_confirm.png)

### Activación del menú de lupa Electoral
* Para activar el menú de la lupa en el navbar, debemos setear a true el setting con key: show_lupa_menu
* ![Truncado](/readme_images/setting_show_lupa.png)

Pasos a correr para preparar la lupa constituyente:

PgSearch::Document.destroy_all
Error.destroy_all
Aporte.destroy_all
Aportante.destroy_all
Candidato.destroy_all
Partido.destroy_all
Pacto.destroy_all

Region.all.map{|r| r.nombre = 'DE ' + r.nombre.upcase; r.save}
Region.all.map{|r| r.nombre = r.nombre.gsub('Á', 'A'); r.save}
Region.all.map{|r| r.nombre = r.nombre.gsub('É', 'E'); r.save}
Region.all.map{|r| r.nombre = r.nombre.gsub('Í', 'I'); r.save}

# DEL BIOBIO MAULE LIBERTADOR
# aisen -> aysen


Import pactos ok
Import partidos ok
%w(AYMARA QUECHUA ATACAMEÑO CHANGOS COLLA DIAGUITA RAPA\ NUI MAPUCHE KAWESQAR YAGAN).sort.map{|p| Pueblo.create(:nombre => p)}
Distrito.create(:nombre => 'SIN DISTRITO', :circunscripcion => Circunscripcion.first)

#añadir columna para candidatos ENLACE_DECLARACIÓN_INTERESES
#añadir columna para candidatos ENLACES_EXTERNOS
#añadir columna para candidatos PUEBLO
#todos los candidatos deben estar en la misma pestaña
#todos los candidatos deben tener informada la columna PARTIDO aquellos que no tengan partido debe ser INDEPENDENTES
#todos los candidatos deben tener informada la columna PACTO aquellos que no tengan pacto debe ser "SIN PACTO"
