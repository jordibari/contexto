require 'test_helper'

class LupaControllerTest < ActionDispatch::IntegrationTest
  test "should get index" do
    get lupa_index_url
    assert_response :success
  end

  test "should get comuna" do
    get lupa_comuna_url
    assert_response :success
  end

  test "should get candidato" do
    get lupa_candidato_url
    assert_response :success
  end

end
