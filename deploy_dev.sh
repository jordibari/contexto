#!/bin/bash
datetime_1=$(date)
user=$(whoami)
USER="deploy"
APP_PATH="/home/deploy/contexto"
APP_ENV="development"
BRANCH="development"
LOG_FILE="$APP_PATH/log/deploy.log"
echo "$datetime_1 called redeploy_dev.sh as user: $user" >> $LOG_FILE 2>&1
echo "script: "$0
echo "script params count: "$#
echo "script params: "$@

echo "              switching to $BRANCH branch" >> $LOG_FILE 2>&1
su - "$USER" -c "cd $APP_PATH && git checkout -B $BRANCH" >> $LOG_FILE 2>&1

echo "              updating code from main repo: git pull" >> $LOG_FILE 2>&1
su - "$USER" -c "cd $APP_PATH && git pull origin $BRANCH" >> $LOG_FILE 2>&1

echo "              installing missing gems: bundle install" >> $LOG_FILE 2>&1
su - "$USER" -c 'export PATH=/home/deploy/.rbenv/bin:$PATH && eval "$(rbenv init -)" && cd /home/deploy/contexto && bundle install' >> $LOG_FILE 2>&1

# service nginx stop >> $LOG_FILE 2>&1
#
# echo "              syncronizing database rails db:reset" >> $LOG_FILE 2>&1
# su - "$USER" -c ' export PATH=/home/deploy/.rbenv/bin:$PATH && eval "$(rbenv init -)" && cd /home/deploy/contexto &&  RAILS_ENV=$APP_ENV rails db:migrate' >> $LOG_FILE 2>&1
#
# service nginx start >> $LOG_FILE 2>&1

# echo "              reseting database rails db:migrate" >> $LOG_FILE 2>&1
# su - "$USER" -c 'cd $APP_PATH && RAILS_ENV=$APP_ENV rails db:reset' >> $LOG_FILE 2>&1

echo "              syncronizing js libraries with yarn" >> $LOG_FILE 2>&1
su - "$USER" -c 'cd $APP_PATH && yarn install' >> $LOG_FILE 2>&1

echo "              restarting server: service nginx restart" >> $LOG_FILE 2>&1
service nginx restart >> $LOG_FILE 2>&1
# su - "$USER" -c "sudo nginx -s reload" >> $LOG_FILE 2>&1

datetime_2=$(date)
echo "      execution took $(( $(date -d "$date2" "+%s") - $(date -d "$date1" "+%s") )) seconds" >> $LOG_FILE 2>&1
