Rails.application.routes.draw do
  get 'lupa/index'
  get 'comuna/:comuna_id' => 'lupa#comuna', as: 'comuna'
  get 'candidato/:candidato_id' => 'lupa#candidato', as: 'candidato'
  get 'aportes_candidato/:candidato_id' => 'lupa#aportes_candidato', as: 'aportes_candidato'
  get 'aportante/:aportante_id' => 'lupa#aportante', as: 'aportante'
  get 'denuncias' => 'lupa#denuncias', as: 'denuncias'
  get 'descargas' => 'lupa#descargas', as: 'descargas'
  post 'lupa/search' => 'lupa#search'
  get 'home/search'
  get 'home/who'
  get 'home/factual'
  get 'home/agenda'
  get 'home/metodo'
  get 'home/consejo'
  get 'home/event/:id' => 'home#event', as: 'event'
  get 'recursos/index'
  get 'recurso/:id' => 'recursos#show', as: 'recurso'
  get 'category/:id' => 'recursos#category', as: 'category'
  get 'post_category/:id' => 'posts#post_category', as: 'post_category'
  get 'section/:id' => 'recursos#section', as: 'section'
  get 'landing' => 'recursos#landing', as: 'landing'
  get 'faqs' => 'faqs#index', as: 'faqs'
  devise_for :users
  mount RailsAdmin::Engine => '/admin', as: 'rails_admin'
  root to: "home#index"
  resources :posts
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
  # resources :youtube, only: :show
end
