RailsAdmin.config do |config|

  require Rails.root.join('lib', 'rails_admin.rb')
  config.main_app_name = ["PlataformaContexto", "Admin"]

  ### Popular gems integration

  ## == Devise ==
  config.authenticate_with do
    warden.authenticate! scope: :user
  end
  config.current_user_method(&:current_user)

  ## == CancanCan ==
  # config.authorize_with :cancancan

  ## == Pundit ==
  # config.authorize_with :pundit

  ## == PaperTrail ==
  # config.audit_with :paper_trail, 'User', 'PaperTrail::Version' # PaperTrail >= 3.0.0

  ### More at https://github.com/sferik/rails_admin/wiki/Base-configuration

  ## == Gravatar integration ==
  ## To disable Gravatar integration in Navigation Bar set to false
  # config.show_gravatar = true

  config.actions do
    dashboard
    # processing
    index                         # mandatory
    new
    export
    bulk_delete
    show
    edit
    delete
    show_in_app
    all
    import
    truncate do
      my_option :another_value
    end

    ## With an audit adapter, you can add:
    # history_index
    # history_show
  end

  # Default global RailsAdminImport options
  config.configure_with(:import) do |config|
    config.logging = true
    config.line_item_limit = 10000
    config.update_if_exists = false
    config.rollback_on_error = false
    config.header_converter = lambda do |header|
      header.parameterize.underscore if header.present?
    end
    config.csv_options = {}
  end
end
