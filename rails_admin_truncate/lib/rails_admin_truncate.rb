require "rails_admin_truncate/engine"

module RailsAdminTruncate
  # Your code goes here...
end

require 'rails_admin/config/actions'

module RailsAdmin
  module Config
    module Actions
      class Truncate < Base
        RailsAdmin::Config::Actions.register(self)

        register_instance_option :root? do
          false
        end

        register_instance_option :collection do
          true
        end

        register_instance_option :object_level do
          false
        end

        register_instance_option :my_option do
           :default_value
        end

        register_instance_option :link_icon do
          'icon-warning-sign'
        end

        # This block is evaluated in the context of the controller when action is called
        # You can access:
        # - @objects if you're on a model scope
        # - @abstract_model & @model_config if you're on a model or object scope
        # - @object if you're on an object scope
        register_instance_option :controller do
          proc do
            permitted_params = params.permit(:truncate_sure, :model_name)
            next_action = @action
            if permitted_params[:truncate_sure] == "1"
              permitted_params[:model_name].classify.constantize.destroy_all
              flash[:notice] = permitted_params[:model_name].classify + " table has been truncated"
              # next_action = RailsAdmin::Config::Actions.find(:index)
              # redirect_to next_action
            end
            render action: next_action.template_name
            # redirect_to next_action
          end
        end

      end
    end
  end
end
