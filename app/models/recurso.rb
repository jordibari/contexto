class Recurso < ApplicationRecord
  belongs_to :recurso_category
  has_one_attached :document
  has_one_attached :picture

  validate :fields_start_with_https

  attribute :remove_picture, :boolean
  after_save :delete_picture, if: -> { remove_picture.present? }
  attribute :remove_document, :boolean
  after_save :delete_document, if: -> { remove_document.present? }


  include PgSearch::Model
  multisearchable against: [:description_long, :name, :resume, :author]

  Gutentag::ActiveRecord.call self

  def delete_document
    self.document.purge
  end

  def delete_picture
    self.picture.purge
  end

  def fields_start_with_https
    campos = %w(url facebook twitter instagram)

    campos.each do |campo|
      if !self.attributes[campo].blank?
        unless self.attributes[campo].start_with?('https://') || self.attributes[campo].start_with?('http://')
          errors.add(campo.to_sym, " debe comenzar con http:// o https://")
        end
      end
    end
  end

  RailsAdmin.config do |config|
    config.model 'Recurso' do
      edit do
        field :recurso_category
        field :name, :string
        field :description_short, :text
        field :description_long, :text
        field :author
        field :resume
        field :fecha
        field :recommended
        field :reading_time
        field :dificulty
        field :url
        field :video
        field :facebook
        field :twitter
        field :instagram
        field :document
        field :picture
        field :tags
        # field :taggings
      end
    end
  end
end
