class Pueblo < ApplicationRecord
  has_many :candidatos

  RailsAdmin.config do |config|
    config.model 'Pueblo' do
      parent Candidato
      object_label_method :nombre
      list do
        field :id
        field :nombre
        items_per_page 100
      end
    end
  end
end
