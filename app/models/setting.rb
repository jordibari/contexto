class Setting < ApplicationRecord


  def self.get(key)
    self.where(key: key).first.nil? ? '' : self.where(key: key).first.value
  end

  RailsAdmin.config do |config|
    config.model 'Setting' do
      field :section
      field :key, :string
      field :value, :text
    end
  end
end
