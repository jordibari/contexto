class Post < ApplicationRecord
  has_rich_text :content
  has_one_attached :image
  belongs_to :qualification, optional: true
  belongs_to :post_category

  attribute :remove_image, :boolean
  after_save :delete_image, if: -> { remove_image.present? }

  Gutentag::ActiveRecord.call self

  include PgSearch::Model
  multisearchable against: [:content, :author, :title, :resume]

  scope :published, ->(time) { where(["fecha <= :time and borrador = :draft", { time: time, draft: false}]).order(fecha: :desc) }

  def self.nota_destacada
    notas_publicadas = PostCategory.find_by(name: "Notas").posts.published(Date.today)

    nota = notas_publicadas.where(destacado: true).first  || notas_publicadas.first
    nota
  end

  RailsAdmin.config do |config|
    config.model 'Post' do
      list do
          field :id
          field :title
          field :fecha
          field :post_category
          field :destacado
          field :borrador
          sort_by :fecha
          items_per_page 100
        end
      edit do
        field :post_category
        field :title
        field :destacado
        field :resume
        field :author
        field :fecha
        field :image
        field :qualification
        field :content do
          js_location { bindings[:view].asset_pack_path 'actiontext.js' }
        end
        field :tags
        field :borrador
        # field :taggings
      end
    end
  end
end
