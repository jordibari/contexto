class Aporte < ApplicationRecord
  belongs_to :candidato
  belongs_to :aportante

  enum tipo_de_aporte: {  propio: 0,
                          credito: 1,
                          partido_politico: 2,
                          no_publico: 3,
                          publico: 4,
                          anticipo_fiscal: 5,
                          con_publicidad: 6,
                          sin_publicidad: 7,
                          credito_con_mandato: 8
                           }

  def tipo_aporte_correcto
    tipos = %w(PROPIO Crédito Partido\ Político No\ Público Público Anticipo\ Fiscal CON\ PUBLICIDAD SIN\ PUBLICIDAD CREDITO\ CON\ MANDATO)
    tipos[Aporte.tipo_de_aportes[self.tipo_de_aporte]]
  end

  def self.before_import
    # called on the model class once before importing any individual records
  end

  def self.before_import_find(record)
    # called on the model class before finding or creating the new record
    # maybe modify the import record that will be used to find the model
    # throw :skip to skip importing this record

    # p record.to_yaml
    nombre_aportante = record[:nombre_aportante] || ""
    record[:aportante] = Aportante.find_or_create_by(nombre: nombre_aportante.strip)
    nombre_aportado = record[:nombre_candidato] || ""
    record[:candidato] = Candidato.find_by(nombre: nombre_aportado.strip)
    record[:tipo_de_aporte] = record[:tipo_de_aporte].parameterize.underscore.to_sym
    # tmp_date = record[:fecha].split("/")
    # record[:fecha] = Date.new(tmp_date[2], tmp_date[1], tmp_date[0])
    record[:fecha] = record[:fecha_de_transferencia]
    record[:monto]

    # p record.to_yaml
    # throw :skip unless record[:email] && record[:email].ends_with? "@mycompany.com"
  end

  def before_import_attributes(record)
    # called on the blank new model or the found model before fields are imported
    # maybe delete fields from the import record that you don't need
    # throw :skip to skip importing this record
  end

  def before_import_associations(record)
    # called on the model with attributes but before associations are imported
    # do custom import of associations
    # make sure to delete association fields from the import record to avoid double import
    # p "++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++"
    # pacto = Pacto.find_or_create_by!(nombre: record[:pacto])
    # record.delete(:pacto)
    # partido = Partido.find_or_create_by!(nombre: record[:partido])
    # record.delete(:partido)
    # record[:partido] = partido
    # # record[:pacto_id] = pacto.id
    # # pacto = Pacto.find_or_create_by nombre: record[:pacto]
    # distrito = Distrito.find_or_create_by!(nombre: record[:distrito].to_i.to_s)
    # record.delete(:distrito)
    # record[:distrito] = distrito
    # p "++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++"
    # p record.to_yaml
    # p "++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++"
    # throw :skip to skip importing this record
  end

  def before_import_save(record)
    # called on the model before it is saved but after all fields and associations have been imported
    # make final modifications to the record
    # throw :skip to skip importing this record
  end

  def after_import_save(record)
    # called on the model after it is saved
  end

  def after_import_association_error(record)
    # called on the model when an association cannot be found
  end

  def after_import_error(record)
    # called on the model when save fails
    Error.create(modelo: "Aporte", registro: record, errores: self.errors.full_messages.to_s)
  end

  def self.after_import
    # called once on the model class after importing all individual records
    # p "******************************************************************************************************************************"
    # p "******************************************************************************************************************************"
    # p self.inspect
    # p "******************************************************************************************************************************"
    # p "******************************************************************************************************************************"
  end

  RailsAdmin.config do |config|
    config.model 'Aporte' do
      parent 'Candidato'
      list do
          field :id
          field :fecha
          field :monto
          field :candidato do
            queryable true
            searchable :nombre
          end
          field :aportante do
            queryable true
            searchable :nombre
          end
          field :tipo_de_aporte
          items_per_page 100
        end
    end
  end

end
