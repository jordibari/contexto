class Qualification < ApplicationRecord
  has_many :posts

  RailsAdmin.config do |config|
    config.model 'Qualification' do
      parent Post
    end
  end
end
