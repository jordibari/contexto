class Distrito < ApplicationRecord
  belongs_to :circunscripcion
  has_many :comunas
  has_many :candidatos


  def aportes_x_candidato
    Rails.cache.fetch("#{self.cache_key}/aportes_x_tipo_1", expires_in: 24.hours) do
      tmp = []
      self.candidatos.each do |candidato|
        tmp << [candidato.nombre, (candidato.aportes.sum(:monto) / 1000000).round(2), "/candidato/"+candidato.id.to_s, candidato.pacto.color || '#777777', candidato.pacto.nombre, ActiveSupport::NumberHelper::number_to_delimited(candidato.aportes.sum(:monto), delimiter: '.', separator: ',')]
      end
      tmp.sort_by { |k| -k[1].to_i }
    end
  end

  RailsAdmin.config do |config|
    config.model 'Distrito' do
      parent Circunscripcion
      object_label_method :nombre
      list do
        field :id
        field :nombre
        field :circunscripcion
        field :comunas
        items_per_page 100
      end
    end
  end
end
