class Candidato < ApplicationRecord
  belongs_to :partido, optional: true
  belongs_to :pacto, optional: true
  belongs_to :distrito, optional: true
  has_many :aportes
  belongs_to :pueblo, optional: true

  include PgSearch::Model
  multisearchable against: [:nombre], using: [:tsearch]

  def self.before_import
    # called on the model class once before importing any individual records
  end

  def self.before_import_find(record)
    # called on the model class before finding or creating the new record
    # maybe modify the import record that will be used to find the model
    # throw :skip to skip importing this record

    p record.to_yaml
    p record
    record.delete(:region)
    record.delete(:aporte_de_partido_estimado)
    record[:nombre] = record[:nombre_candidato].strip
    record.delete(:nombre_candidato)
    if record.has_key? :enlace_declaracion_intereses
      record[:intereses] = record[:enlace_declaracion_intereses].strip unless record[:enlace_declaracion_intereses].nil?
    end
    # record.delete(:enlace_declaracion_intereses)
    if record.has_key? :enlaces_externos
      record[:url] = record[:enlaces_externos].strip unless record[:enlaces_externos].nil?
    end
    # record.delete(:enlaces_externos)
    # throw :skip unless record[:email] && record[:email].ends_with? "@mycompany.com"
  end

  def before_import_attributes(record)
    # called on the blank new model or the found model before fields are imported
    # maybe delete fields from the import record that you don't need
    # throw :skip to skip importing this record
  end

  def before_import_associations(record)
    # called on the model with attributes but before associations are imported
    # do custom import of associations
    # make sure to delete association fields from the import record to avoid double import
    # p "++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++"
    nombre_pacto = record[:pacto] || ""
    pacto = Pacto.find_by(nombre: nombre_pacto.strip)
    record[:pacto] = pacto

    nombre_partido = record[:partido] || ""
    partido = Partido.find_by(nombre: nombre_partido.strip)
    record[:partido] = partido

    nombre_pueblo = record[:pueblo] || ""
    pueblo = Pueblo.find_by(nombre: nombre_pueblo.strip)
    record[:pueblo] = pueblo

    if record[:distrito].to_s.include?('.')
      nombre_distrito = record[:distrito].to_i.to_s || ""
    else
      nombre_distrito = record[:distrito].to_s || ""
    end
    p "++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++"
    p nombre_distrito
    p "++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++"
    distrito = Distrito.find_by(nombre: nombre_distrito.strip)
    record[:distrito] = distrito

    # # record[:pacto_id] = pacto.id
    # # pacto = Pacto.find_or_create_by nombre: record[:pacto]
    # distrito = Distrito.find_or_create_by!(nombre: record[:distrito].to_i.to_s)
    # record.delete(:distrito)
    # record[:distrito] = distrito
    # p "++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++"
    # p record.to_yaml
    # p "++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++"
    # throw :skip to skip importing this record
  end

  def before_import_save(record)
    # called on the model before it is saved but after all fields and associations have been imported
    # make final modifications to the record
    # throw :skip to skip importing this record
  end

  def after_import_save(record)
    # called on the model after it is saved
  end

  def after_import_association_error(record)
    # called on the model when an association cannot be found
  end

  def after_import_error(record)
    # called on the model when save fails
    Error.create(modelo: "Candidato", registro: record, errores: self.errors.full_messages.to_s)
  end

  def self.after_import
    # called once on the model class after importing all individual records
  end
  def c_humanas
    self.compromiso ? 'SI' : 'NO'
  end

  def tipo_aporte_legible(tipo)
    tipos = %w(PROPIO Crédito Partido\ Político No\ Público Público Anticipo\ Fiscal CON\ PUBLICIDAD SIN\ PUBLICIDAD CREDITO\ CON\ MANDATO)
    tipos[Aporte.tipo_de_aportes[tipo]]
  end

  def aportes_acumulados_x_dia
    Rails.cache.fetch("#{self.cache_key}/aportes_acumulados_x_dia", expires_in: 24.hours) do
      tmp = {}
      tmp_monto = 0
      tmp_limite = {}
      self.aportes.all.group_by_day(:fecha).sum(:monto).each do |grupo|
        tmp_monto = tmp_monto + grupo[1]
        tmp.merge! grupo[0] => (tmp_monto / 1000000).round(2)
        tmp_limite.merge! grupo[0] => (self.distrito.limite_gasto / 1000000).round(2)
      end
      [{"name": "Aportes acumulados", "data": tmp}, {"name": "Límite", "data": tmp_limite }]
    end
  end

  def aportes_x_tipo
    Rails.cache.fetch("#{self.cache_key}/aportes_x_tipo", expires_in: 24.hours) do
      tmp = []
      tmp_monto = 0
      self.aportes.all.group_by { |a| a.tipo_de_aporte }.each do |grupo|
        tmp_monto = 0
        grupo[1].map{|aporte| tmp_monto += aporte.monto}
        tmp << [ tipo_aporte_legible(grupo[0]) , tmp_monto]
      end
      tmp
    end
  end

  def self.con_mas_aportes(num)
    Candidato.all.map{|c| {:id => c.id, :distrito => c.distrito.nombre, :nombre => c.nombre, :monto => c.aportes.sum(:monto)}}.sort_by.sort_by { |k| -k[:monto] }.first(num)
  end

  def self.chart_mas_aportes(num)

    # cache_key = Aporte.last.id.to_s + "--" + Aporte.last.updated_at.to_s
    cache_key = self.cache_key_with_version
    # p cache_key
    Rails.cache.fetch("#{cache_key}/#{num}/chart_aportes", expires_in: 24.hours) do
      Candidato.all.map{|c| [c.nombre, (c.aportes.sum(:monto).to_f/1000000).round(2), "/candidato/"+c.id.to_s, c.pacto.color || '#777777', c.pacto.nombre, ActiveSupport::NumberHelper::number_to_delimited(c.aportes.sum(:monto), delimiter: '.', separator: ',')]}.sort_by.sort_by { |k| -k[1] }.first(num)
    end
  end

  def aportes_cached
    # p self.cache_key
    Rails.cache.fetch("#{self.cache_key}/aportes_cached", expires_in: 24.hours) do
      # self.aportes.includes(:aportantes).order(:fecha)
      Aporte.includes(:aportante).where(candidato: self)
    end
  end

  def tabla_aportes_cached
    # p self.cache_key
    Rails.cache.fetch("#{self.cache_key}/tabla_aportes_cached", expires_in: 24.hours) do
      # self.aportes.includes(:aportantes).order(:fecha)
      aportes = []
      Aporte.includes(:aportante).where(candidato: self).each do |a|
        b = {:aportante => {:id => a.aportante.id, :nombre => a.aportante.nombre}, :monto => ActiveSupport::NumberHelper::number_to_delimited(a.monto, delimiter: '.', separator: ','), :fecha => a.fecha, :tipo => tipo_aporte_legible(a.tipo_de_aporte)}
        aportes << b
      end
      aportes
    end
  end

  def to_s
    self.nombre
  end

  def self.cache_key_with_version
    Aporte.count > 0 ? "Candidato/last_aporte-" + Aporte.last.id.to_s : ''
  end

  RailsAdmin.config do |config|
    config.model 'Candidato' do
      object_label_method :nombre
      list do
          field :id
          field :nombre
          field :distrito do
            queryable true
            searchable :nombre
          end
          field :pueblo
          field :partido do
            queryable true
            searchable :nombre
          end
          field :pacto do
            queryable true
            searchable :nombre
          end
          items_per_page 100
        end
    end
  end

end
