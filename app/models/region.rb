class Region < ApplicationRecord
  has_many :provincias
  has_many :comunas, through: :provincias

  RailsAdmin.config do |config|
    config.model 'Region' do
      parent Candidato
      # parent 'Candidato'
      object_label_method :nombre
      list do
        field :id
        field :nombre
        field :provincias
        items_per_page 100
      end
    end
  end
end
