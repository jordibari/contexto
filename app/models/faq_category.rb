class FaqCategory < ApplicationRecord

  RailsAdmin.config do |config|
    config.model 'FaqCategory' do
      parent Faq

      list do
        field :id
        field :name
        field :order
      end
    end
  end
end
