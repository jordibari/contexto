class Circunscripcion < ApplicationRecord
  has_many :distritos

  RailsAdmin.config do |config|
    config.model 'Circunscripcion' do
      parent Region
      object_label_method :nombre
      list do
        field :id
        field :nombre
        field :distritos
        items_per_page 100
      end
    end
  end
end
