class Section < ApplicationRecord
  has_many :recurso_categories
  has_one_attached :icon
end
