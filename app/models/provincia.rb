class Provincia < ApplicationRecord
  belongs_to :region
  has_many :comunas

  RailsAdmin.config do |config|
    config.model 'Provincia' do
      parent Region
      object_label_method :nombre
      list do
        field :id
        field :nombre
        field :region
        field :comunas
        items_per_page 100
      end
    end
  end
end
