class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable, :omniauthable, :registerable
  devise :database_authenticatable,
         :recoverable, :rememberable, :validatable

   RailsAdmin.config do |config|
     config.model 'User' do
        visible false
       end
   end
end
