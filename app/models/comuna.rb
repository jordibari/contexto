class Comuna < ApplicationRecord
  belongs_to :provincia
  belongs_to :distrito
  has_many :lugars

  include PgSearch::Model
  multisearchable against: [:nombre]  

  RailsAdmin.config do |config|
    config.model 'Comuna' do
      parent Provincia
      object_label_method :nombre
      list do
        field :id
        field :nombre
        field :provincia
        field :distrito
        items_per_page 100
      end
    end
  end
end
