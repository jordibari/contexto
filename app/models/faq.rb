class Faq < ApplicationRecord
  belongs_to :faq_category
  
  include PgSearch::Model
  multisearchable against: [:question, :answer]
end
