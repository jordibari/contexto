class Organization < ApplicationRecord
  has_many :members
  has_one_attached :picture

  enum tipo: {  directivo: 0,
                participante: 1,
                colaborador: 2 }


  RailsAdmin.config do |config|
    config.model 'Organization' do
      edit do
        field :tipo
        field :name, :string
        field :description, :text
        field :url
        field :picture
        field :members
      end
    end
  end
end
