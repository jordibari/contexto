class Member < ApplicationRecord
  belongs_to :organization
  has_one_attached :picture

  RailsAdmin.config do |config|
    config.model 'Member' do
      parent Organization
    end
  end
end
