class InfoDenuncium < ApplicationRecord


  def agg_sum
    self.admisibles + self.inadmisibles + self.en_analisis
  end


  def self.before_import
    # called on the model class once before importing any individual records
  end

  def self.before_import_find(record)
    # called on the model class before finding or creating the new record
    # maybe modify the import record that will be used to find the model
    # throw :skip to skip importing this record
    # throw :skip unless record[:email] && record[:email].ends_with? "@mycompany.com"
  end

  def before_import_attributes(record)
    # called on the blank new model or the found model before fields are imported
    # maybe delete fields from the import record that you don't need
    # throw :skip to skip importing this record
    p "++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++"
    p record
    record[:original_record] = record
    record[:admisibles] = record[:admisible]
    record.delete(:admisible)
    record[:inadmisibles] = record[:inadmisible]
    record.delete(:inadmisible)
    record[:descripcion] = record[:descripcion].downcase

  end

  def before_import_associations(record)
    # called on the model with attributes but before associations are imported
    # do custom import of associations
    # make sure to delete association fields from the import record to avoid double import

    # throw :skip to skip importing this record
  end

  def before_import_save(record)
    # called on the model before it is saved but after all fields and associations have been imported
    # make final modifications to the record
    p record
    # throw :skip to skip importing this record
  end

  def after_import_save(record)
    # called on the model after it is saved
  end

  def after_import_association_error(record)
    # called on the model when an association cannot be found
  end

  def after_import_error(record)
    # called on the model when save fails
    Error.create(modelo: "InfoDenuncium", registro: record, errores: self.errors.full_messages.to_s)
  end

  def self.after_import
    # called once on the model class after importing all individual records
  end

  def self.denuncias_cached
    Rails.cache.fetch("denuncias_cached", expires_in: 24.hours) do
      InfoDenuncium.all.sort_by(&:agg_sum).reverse
    end
  end


  def self.chart_mas_denuncias(num)

    # cache_key = Aporte.last.id.to_s + "--" + Aporte.last.updated_at.to_s
    cache_key = self.cache_key_with_version
    # p cache_key
    Rails.cache.fetch("#{cache_key}/#{num}/chart_mas_denuncias", expires_in: 24.hours) do
      # Candidato.all.map{|c| [c.nombre, (c.aportes.sum(:monto).to_f/1000000).round(2)]}.sort_by.sort_by { |k| -k[1] }.first(num)
      InfoDenuncium.all.map{|i| [i.descripcion, i.agg_sum]}.sort_by.sort_by { |k| -k[1] }.first(num)
    end
  end

  def self.denuncias_x_tipo

    # cache_key = Aporte.last.id.to_s + "--" + Aporte.last.updated_at.to_s
    cache_key = self.cache_key_with_version
    # p cache_key
    Rails.cache.fetch("#{cache_key}/denuncias_x_tipo", expires_in: 24.hours) do
      # Candidato.all.map{|c| [c.nombre, (c.aportes.sum(:monto).to_f/1000000).round(2)]}.sort_by.sort_by { |k| -k[1] }.first(num)
      InfoDenuncium.all.map{|i| [i.descripcion, i.agg_sum]}.sort_by { |k| -k[1] }
    end
  end

  def self.cache_key_with_version
     "Denuncias/last_-" + InfoDenuncium.last.id.to_s
  end


  RailsAdmin.config do |config|
    config.model 'InfoDenuncium' do
      parent Candidato
    end
  end
end
