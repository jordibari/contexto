class RecursoCategory < ApplicationRecord
  has_many :recursos
  has_one_attached :icon
  has_one_attached :picture
  belongs_to :section

  enum box_size: {  normal: -1,
                    big: 0,
                    small: 1 }


  attribute :remove_picture, :boolean
  after_save :delete_picture, if: -> { remove_picture.present? }
  attribute :remove_icon, :boolean
  after_save :delete_icon, if: -> { remove_icon.present? }

  def delete_icon
    self.icon.purge
  end

  def delete_picture
    self.picture.purge
  end

  def red
    has_color
    self.color.slice(0,2)
  end

  def green
    has_color
    self.color.slice(2,2)
  end

  def blue
    has_color
    self.color.slice(4,2)
  end

  def has_color
    self.color = '000000' if self.color.blank?
  end

  RailsAdmin.config do |config|
    config.model 'RecursoCategory' do
      parent Recurso
      edit do
        field :section
        field :order
        field :name, :string
        field :description_short, :text
        field :icon_text, :string
        field :box_size
        field :icon
        field :picture
        field :recursos
      end
    end
  end
end
