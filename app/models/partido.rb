class Partido < ApplicationRecord
  # belongs_to :pacto
  has_many :candidatos

  validates :nombre, uniqueness: true, length: { minimum: 2 }

  def self.before_import
    # called on the model class once before importing any individual records
  end

  def self.before_import_find(record)
    # called on the model class before finding or creating the new record
    # maybe modify the import record that will be used to find the model
    # throw :skip to skip importing this record
    # throw :skip unless record[:email] && record[:email].ends_with? "@mycompany.com"
    # throw :skip if Partido.exists?(:nombre => record[:nombre])
  end

  def before_import_attributes(record)
    # called on the blank new model or the found model before fields are imported
    # maybe delete fields from the import record that you don't need
    # throw :skip to skip importing this record
  end

  def before_import_associations(record)
    # called on the model with attributes but before associations are imported
    # do custom import of associations
    # make sure to delete association fields from the import record to avoid double import
    # throw :skip to skip importing this record
  end

  def before_import_save(record)
    # called on the model before it is saved but after all fields and associations have been imported
    # make final modifications to the record
    # throw :skip to skip importing this record
    self.nombre = record[:partido].strip unless record[:partido].nil?
    record = {}
    throw :skip if Partido.exists?(:nombre => self.nombre)
    throw :skip if self.nombre.nil?
  end

  def after_import_save(record)
    # called on the model after it is saved
  end

  def after_import_association_error(record)
    # called on the model when an association cannot be found
  end

  def after_import_error(record)
    # called on the model when save fails
  end

  def self.after_import
    # called once on the model class after importing all individual records
  end

  RailsAdmin.config do |config|
    config.model 'Partido' do
      parent Candidato
      object_label_method :nombre
      list do
          field :id
          field :nombre
          items_per_page 100
        end
    end
  end
end
