class PostCategory < ApplicationRecord
  has_many :posts

  RailsAdmin.config do |config|
    config.model 'PostCategory' do
      parent Post
      edit do
        field :name, :string
        field :description, :text
        field :posts
      end
    end
  end
end
