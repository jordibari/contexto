class Aportante < ApplicationRecord
  has_many :aportes

  include PgSearch::Model
  multisearchable against: [:nombre]

  def self.before_import
    # called on the model class once before importing any individual records
  end

  def self.before_import_find(record)
    # called on the model class before finding or creating the new record
    # maybe modify the import record that will be used to find the model
    # throw :skip to skip importing this record

    p record.to_yaml
    record.delete(:region)
    record.delete(:aporte_de_partido_estimado)
    record[:nombre] = record[:nombre_candidato]
    record.delete(:nombre_candidato)
    record[:intereses] = record[:enlace_declaracion_intereses]
    record.delete(:enlace_declaracion_intereses)
    record[:url] = record[:enlaces_externos]
    record.delete(:enlaces_externos)
    # throw :skip unless record[:email] && record[:email].ends_with? "@mycompany.com"
  end

  def before_import_attributes(record)
    # called on the blank new model or the found model before fields are imported
    # maybe delete fields from the import record that you don't need
    # throw :skip to skip importing this record
  end

  def before_import_associations(record)
    # called on the model with attributes but before associations are imported
    # do custom import of associations
    # make sure to delete association fields from the import record to avoid double import
    # p "++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++"
    # pacto = Pacto.find_or_create_by!(nombre: record[:pacto])
    # record.delete(:pacto)
    # partido = Partido.find_or_create_by!(nombre: record[:partido])
    # record.delete(:partido)
    # record[:partido] = partido
    # # record[:pacto_id] = pacto.id
    # # pacto = Pacto.find_or_create_by nombre: record[:pacto]
    # distrito = Distrito.find_or_create_by!(nombre: record[:distrito].to_i.to_s)
    # record.delete(:distrito)
    # record[:distrito] = distrito
    # p "++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++"
    # p record.to_yaml
    # p "++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++"
    # throw :skip to skip importing this record
  end

  def before_import_save(record)
    # called on the model before it is saved but after all fields and associations have been imported
    # make final modifications to the record
    # throw :skip to skip importing this record
  end

  def after_import_save(record)
    # called on the model after it is saved
  end

  def after_import_association_error(record)
    # called on the model when an association cannot be found
  end

  def after_import_error(record)
    # called on the model when save fails
    self.errors.add(:base, :name_or_email_blank,  message: "either name or email must be present")
  end

  def self.after_import
    # called once on the model class after importing all individual records
  end

  def self.chart_mas_aportes(num)
    # Aporte.all.group_by(:aportante)

    cache_key = self.cache_key_with_version
    # p cache_key
    Rails.cache.fetch("#{cache_key}/#{num}/chart_aportantes", expires_in: 24.hours) do
      Aportante.where.not(:nombre => '').map{|a| [a.nombre, (a.aportes.sum(:monto).to_f/1000000).round(2), "/aportante/"+a.id.to_s] }.sort_by.sort_by { |k| -k[1] }.first(num)
    end
  end

  def aportes_cached
    # p self.cache_key
    Rails.cache.fetch("#{self.cache_key}/aportes_cached", expires_in: 24.hours) do
      # self.aportes.includes(:aportantes).order(:fecha)
      Aporte.includes(:candidato).where(aportante: self)
    end
  end

  def self.cache_key_with_version
     # "/last_aporte-" + Aporte.last.id.to_s
     Aporte.count > 0 ? "Aportante/last_aporte-" + Aporte.last.id.to_s : ''
  end

  RailsAdmin.config do |config|
    config.model 'Aportante' do
      parent 'Aporte'
      object_label_method :nombre
      list do
          field :id
          field :nombre
          items_per_page 100
        end
    end
  end

end
