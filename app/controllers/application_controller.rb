class ApplicationController < ActionController::Base
  include Pagy::Backend
  rescue_from ActionController::RoutingError, with: :goto_root

  private
    def goto_root
      redirect_to root_path
    end
end
