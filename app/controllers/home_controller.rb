class HomeController < ApplicationController
  def index
    @categories = RecursoCategory.all
    @recursos = Recurso.all
  end

  def search
    respond_to do |format|
      format.html do
        value = search_params.slice(:search)
        @results = PgSearch.multisearch(value["search"])
        # p "Results: #{@results}"
        @recursos= []
        @faqs= []
        @posts= []
        @results.each do |r|
          # p "Showing result... #{r}"
          if r.searchable_type == 'Recurso'
            @recursos.push(Recurso.find(r.searchable_id))
          elsif r.searchable_type == 'Faq'
            @faqs.push(Faq.find(r.searchable_id))
          elsif r.searchable_type == 'Post'
            @posts.push(Post.find(r.searchable_id))
          end
        end
        @search_params = value["search"]
      end
    end
  end

  def factual
  end

  def event
    # p event_params[:id]
    @event = Event.find(event_params[:id])
  end

  def metodo
  end

  def consejo
  end

  private

  def search_params
    # p "params"
    # p params
    params.permit(:search)
  end

  def event_params
    # p "params"
    # p params
    params.permit(:id)
  end

end
