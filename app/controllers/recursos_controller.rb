class RecursosController < ApplicationController
  add_breadcrumb "home", :root_path

  def index
     add_breadcrumb "recursos", :recursos_index_path
     @categories = RecursoCategory.all
     @recursos = Recurso.all
  end

  def landing
     @categories = RecursoCategory.all
     @recursos = Recurso.all
  end

  def category
    @category = RecursoCategory.find(category_params[:id])
    @pagy, @records = pagy(@category.recursos.order(id: :desc), items: 3)
    add_breadcrumb "recursos", :recursos_index_path
    add_breadcrumb(@category.name, :category_path)
  end

  def section
    @section = Section.find(section_params[:id])
  end

  def show
    @recurso = Recurso.find(params[:id])
    add_breadcrumb "recursos", :recursos_index_path
    add_breadcrumb(@recurso.recurso_category.name, :category_path)
    add_breadcrumb @recurso.name, :recurso_path
  end

  private

  def category_params
    params.permit(:id)
  end

  def section_params
    params.permit(:id)
  end
end
