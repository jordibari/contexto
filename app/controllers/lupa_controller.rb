class LupaController < ApplicationController
  layout "lupa"

  def index
  end

  def comuna
    @comuna = Comuna.find(params[:comuna_id])
  end

  def candidato
    @candidato = Candidato.find(params[:candidato_id])
  end

  def aportes_candidato
    respond_to do |format|
      format.json do
        @candidato = Candidato.find(params[:candidato_id])
        render json: @candidato.tabla_aportes_cached
      end
    end
  end

  def aportante
    @aportante = Aportante.find(params[:aportante_id])
  end

  def denuncias
    @denuncias = InfoDenuncium.all
  end

  def descargas
    @ficheros = Fichero.all
  end

  def search
    # respond_to do |format|
    #   format.json do
        value = search_params.slice(:search)
        @results = PgSearch.multisearch(value["search"])
        # p "Results: #{@results}"
        @comunas= []
        @candidatos= []
        @aportantes= []
        @results.each do |r|
          # p "Showing result... #{r}"
          if r.searchable_type == 'Comuna'
            @comunas.push(Comuna.find(r.searchable_id))
          elsif r.searchable_type == 'Candidato'
            # p "candidato found!!!!!!!!!!!!"
            @candidatos.push(Candidato.find(r.searchable_id))
          elsif r.searchable_type == 'Aportante'
            @aportantes.push(Aportante.find(r.searchable_id))
          end
        end
        @search_params = value["search"]
        # render json: @candidatos, status: :ok
    #   end
    # end
    render :layout => false
  end

  def search_params
    # p "params"
    # p params
    params.permit(:authenticity_token, :search)
  end

end
