module ApplicationHelper
  include Pagy::Frontend

  def to_pesos(number)
    number_to_currency(number, unit: "$",precision:0, separator: "0", delimiter: ".")
  end
end
