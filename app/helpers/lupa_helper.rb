module LupaHelper
  def tipo_aporte_legible(tipo)
    tipos = %w(PROPIO Crédito Partido\ Político No\ Público Público Anticipo\ Fiscal CON\ PUBLICIDAD SIN\ PUBLICIDAD CREDITO\ CON\ MANDATO)
    tipos[Aporte.tipo_de_aportes[tipo]]
  end
end
