module HomeHelper

  def num_to_month(month_num)
    meses = ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre']
    meses[month_num - 1][0..2]
  end

  def fecha_text(fecha)
    unless fecha.blank?
      fecha.day.to_s + " " + num_to_month(fecha.month)  + " " + fecha.year.to_s
    end
  end
end
