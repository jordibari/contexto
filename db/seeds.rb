# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

user = User.create(email: "admin@example.org", password: "decidim123456", password_confirmation: "decidim123456")
user = User.create(email: "akershenbaum@gmail.com", password: "user_temp_pass", password_confirmation: "user_temp_pass")
user = User.create(email: "jordi.bari@gmail.com", password: "user_temp_pass", password_confirmation: "user_temp_pass")
user = User.create(email: "karen.gaona@mail.udp.cl", password: "user_temp_pass", password_confirmation: "user_temp_pass")
user = User.create(email: "carolina.mosso@espaciopublico.cl", password: "user_temp_pass", password_confirmation: "user_temp_pass")
user = User.create(email: "fgutierrez@humanas.cl", password: "user_temp_pass", password_confirmation: "user_temp_pass")
user = User.create(email: "pacevedo@observatorio.cl", password: "user_temp_pass", password_confirmation: "user_temp_pass")
user = User.create(email: "maria.jaraquemada@espaciopublico.cl", password: "user_temp_pass", password_confirmation: "user_temp_pass")
user = User.create(email: "gabriel.ortiz@espaciopublico.cl", password: "user_temp_pass", password_confirmation: "user_temp_pass")
user = User.create(email: "juanpablo.figueroa@espaciopublico.cl", password: "user_temp_pass", password_confirmation: "user_temp_pass")
user = User.create(email: "valentina.matus@espaciopublico.cl", password: "user_temp_pass", password_confirmation: "user_temp_pass")
user = User.create(email: "manuel.aris@espaciopublico.cl", password: "user_temp_pass", password_confirmation: "user_temp_pass")
user = User.create(email: "hola.fernandacp@gmail.com", password: "user_temp_pass", password_confirmation: "user_temp_pass")

cat_1 = FaqCategory.create(name: "1" + Faker::Lorem.words(3).join(" "))
cat_2 = FaqCategory.create(name: "3" + Faker::Lorem.words(3).join(" "))
cat_3 = FaqCategory.create(name: "2" + Faker::Lorem.words(3).join(" "))
FaqCategory.all.each do |cat|
  Faq.create(faq_category: cat, question: Faker::Lorem.sentence(3), answer: Faker::Lorem.sentence(30))
  Faq.create(faq_category: cat, question: Faker::Lorem.sentence(2), answer: Faker::Lorem.sentence(30))
  Faq.create(faq_category: cat, question: Faker::Lorem.sentence(2), answer: Faker::Lorem.sentence(30))
end

section_1 = Section.create(name: "Infórmate", order: 1)
section_1.icon.attach(io: File.open(Rails.root + 'app/assets/images/informate.svg'), filename: 'image.svg')
section_2 = Section.create(name: "Actívate", order: 2)
section_2.icon.attach(io: File.open(Rails.root + 'app/assets/images/activate.svg'), filename: 'image.svg')

RecursoCategory.create(name: "Opinión Experta", description_short: Faker::Lorem.sentence(2), icon_text: "bong", color: "ff0000", order: 1, section: section_1)
RecursoCategory.create(name: "Minuta Constituyente", description_short: Faker::Lorem.sentence(2), icon_text: "globe", color: "00ff00", order: 2, section: section_1)
RecursoCategory.create(box_size: "small", name: "Material Didáctico", description_short: Faker::Lorem.sentence(2), icon_text: "file-download", color: "0000ff", order: 3, section: section_1)
RecursoCategory.create(name: "Constituciones del Mundo", description_short: Faker::Lorem.sentence(2), icon_text: "file-download", color: "0000ff", order: 4, section: section_1)
RecursoCategory.create(name: "En Cámara", description_short: Faker::Lorem.sentence(2), icon_text: "file-download", color: "0000ff", order: 5, section: section_1)
RecursoCategory.create(box_size: "big", name: "Noticias", description_short: Faker::Lorem.sentence(2), icon_text: "file-download", color: "0000ff", order: 6, section: section_1)
RecursoCategory.create(name: "Preguntas y Respuestas", description_short: Faker::Lorem.sentence(2), icon_text: "file-download", color: "0000ff", order: 7, section: section_1)
RecursoCategory.create(box_size: "small",name: "Actividades", description_short: Faker::Lorem.sentence(2), icon_text: "play-circle", color: "ff00ff", order: 1, section: section_2)
RecursoCategory.create(box_size: "big", name: "Iniciativas Ciudadanas", description_short: Faker::Lorem.sentence(2), icon_text: "microphone-alt", color: "00ffff", order: 2, section: section_2)
RecursoCategory.create(name: "El que baila pasa", description_short: Faker::Lorem.sentence(2), icon_text: "chart-bar", color: "ffff00", order: 3, section: section_2)

RecursoCategory.all.each do |cat|
  cat.icon.attach(io: File.open(Rails.root + 'app/assets/images/activate.svg'), filename: 'image.svg')
  cat.picture.attach(io: File.open(Rails.root + 'app/assets/images/check.svg'), filename: 'image.svg')
  r = Recurso.new(name: Faker::GreekPhilosophers.quote, url: Faker::Internet.url, recurso_category: cat, description_short: Faker::Lorem.sentence(2), description_long: Faker::Lorem.sentence(20), reading_time: 5, dificulty: "Facil", recommended: true, resume: Faker::Lorem.sentence(7), author: Faker::Name.name)
  r.document.attach(io: File.open(Rails.root + 'db/image.png'), filename: 'image.png')
  r.picture.attach(io: File.open(Rails.root + 'db/image.png'), filename: 'image.png')
  r.save
  r_1 = Recurso.new(name: Faker::GreekPhilosophers.quote, url: Faker::Internet.url, recurso_category: cat, description_short: Faker::Lorem.sentence(2), description_long: Faker::Lorem.sentence(20), reading_time: 15, dificulty: "Medio", recommended: true, resume: Faker::Lorem.sentence(7), author: Faker::Name.name)
  r_1.document.attach(io: File.open(Rails.root + 'db/image.png'), filename: 'image.png')
  r_1.picture.attach(io: File.open(Rails.root + 'db/image.png'), filename: 'image.png')
  r_1.save
  r_2 = Recurso.new(name: Faker::GreekPhilosophers.quote, url: Faker::Internet.url, recurso_category: cat, description_short: Faker::Lorem.sentence(2), description_long: Faker::Lorem.sentence(20), reading_time: 55, dificulty: "Dificil", recommended: true, resume: Faker::Lorem.sentence(7), author: Faker::Name.name)
  r_2.document.attach(io: File.open(Rails.root + 'db/image.png'), filename: 'image.png')
  r_2.picture.attach(io: File.open(Rails.root + 'db/image.png'), filename: 'image.png')
  r_2.save
end

Setting.create(section:'home#presentacion', key: 'home_presentacion', value: 'Somos un espacio libre de propaganda política, que entrega información fidedigna, traduciendo contenido teórico complejo y abstracto a nuestro lenguaje y vida cotidiana')
Setting.create(section:'home#recursos', key: 'home_recursos', value: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed eiusmod tempor incidunt ut labore et dolore magna aliqua.')
Setting.create(section:'home#footer', key: 'home_footer', value: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed eiusmod tempor incidunt ut labore et dolore magna aliqua.')
Setting.create(section:'home#footer#newsletter', key: 'footer_newsletter', value: 'A rover wearing a fuzzy suit doesn’t alarm the real penguins')
Setting.create(section:'redes_sociales', key: 'facebook', value: 'https://facebook.com')
Setting.create(section:'redes_sociales', key: 'twitter', value: 'https:/twitter.com')
Setting.create(section:'redes_sociales', key: 'instagram', value: 'https://instagram.com')
Setting.create(section:'shared_message', key: 'shared_message', value: 'comparte plataformacontexto.cl')
Setting.create(section:'quienes somos', key: 'purpose_1_title', value: 'Nuestro propósito')
Setting.create(section:'quienes somos', key: 'purpose_1_content', value: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Ducimus quos quasi ad eligendi temporibus rem totam nisi iste expedita, magnam rerum? Libero sunt a accusamus magnam, temporibus fugit necessitatibus sint?')
Setting.create(section:'quienes somos', key: 'purpose_2_title', value: 'Nuestro propósito')
Setting.create(section:'quienes somos', key: 'purpose_2_content', value: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Ducimus quos quasi ad eligendi temporibus rem totam nisi iste expedita, magnam rerum? Libero sunt a accusamus magnam, temporibus fugit necessitatibus sint?')
Setting.create(section:'quienes somos', key: 'contexto_title', value: 'contexto')
Setting.create(section:'quienes somos', key: 'contexto_content', value: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Ducimus quos quasi ad eligendi temporibus rem totam nisi iste expedita, magnam rerum? Libero sunt a accusamus magnam, temporibus fugit necessitatibus sint?')
Setting.create(section:'quienes somos', key: 'manifiesto_title', value: 'Manifiesto')
Setting.create(section:'quienes somos', key: 'manifiesto_content', value: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Ducimus quos quasi ad eligendi temporibus rem totam nisi iste expedita, magnam rerum? Libero sunt a accusamus magnam, temporibus fugit necessitatibus sint?')
Setting.create(section:'home#agenda', key: 'agenda_title', value: 'Agenda')
Setting.create(section:'home#agenda', key: 'agenda_desc', value: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed eiusmod tempor incidunt ut labore et dolore magna aliqua.')
Setting.create(section:'metodologia', key: 'title_1', value: '1. Selección de las frases a verificar')
Setting.create(section:'metodologia', key: 'content_1', value: 'En este primer paso, el equipo periodístico de Contexto Factual del Laboratorio Constitucional de la Universidad Diego Portales, Espacio Público e Interpreta registra una serie de afirmaciones que diversos líderes de opinión nacional y regionales, y autoridades del oficialismo, oposición, municipios, servicios públicos, entre otros, que realicen respecto al proceso constituyente. Estas pueden emitirse a través de redes sociales, declaraciones en la prensa, discursos, etc., y que son susceptibles de verificación. Deben ser afirmaciones que se remiten a hechos y que son recogidas en forma textual.')
Setting.create(section:'metodologia', key: 'title_2', value: '2. Priorización de las frases de acuerdo con su relevancia e interés público')
Setting.create(section:'metodologia', key: 'content_2', value: 'El equipo revisa las afirmaciones y las selecciona privilegiando aquellas afirmaciones que abordan temas de interés público y cuya verificación son un aporte al debate constituyente. En relación al interés público, el equipo de Contexto Factual seleccionará frases que tengan relevancia social y cuyo esclarecimiento aporten a la calidad del debate público.

Contexto Factual no realiza verificación de afirmaciones que se sustenten en posiciones religiosas, temas del ámbito personal, o en situaciones judiciales que no hayan concluido con una sentencia ejecutoriada. ')
Setting.create(section:'metodologia', key: 'title_3', value: '3. Consulta al entorno de la persona (fuente original)')
Setting.create(section:'metodologia', key: 'content_3', value: ' El equipo periodístico de Contexto Factual se comunica con la persona que dijo la afirmación o bien su grupo de asesores o periodistas para consultar directamente por la fuente en que basó esa afirmación.

La información aportada es corroborada en razón de su validez y contexto. Asimismo, en el caso que la fuente original realice una aclaración sobre la afirmación hecha, se deja constancia pero no cambia el resultado o proceso de la verificación, dado que solo se verifica lo dicho de manera textual.

En caso de no recibir respuesta oportuna continúa el proceso de verificación con los pasos siguientes. ')
Setting.create(section:'metodologia', key: 'title_4', value: '4. Consulta a la fuente oficial')
Setting.create(section:'metodologia', key: 'content_4', value: 'Por medio del acceso a fuentes de datos públicos, el equipo periodístico de Contexto Factual analiza informes, compendios estadísticos, bases de datos y toda documentación que se considere como fuente oficial en la materia tratada. La mayoría de las veces son fuentes gubernamentales a nivel nacional, provincial y local que están a cargo del tema objeto de análisis. La afirmación revisada es contrastada entonces con estas fuentes documentales. Esta puede coincidir o no con la información entregada por la fuente original. ')
Setting.create(section:'metodologia', key: 'title_5', value: '5. Consulta a otras fuentes alternativas')
Setting.create(section:'metodologia', key: 'content_5', value: ' La utilización de otras fuentes es importante para contrarrestar información y confirmar que lo que se ha considerado como la fuente oficial es el mejor dato disponible. Del mismo modo, el uso de datos alternativos, o la opinión de expertos y expertas, puede contribuir al paso siguiente, que es el análisis de las afirmaciones de acuerdo con un contexto. Esta puede coincidir o no con la información entregada por la fuente original.

El uso de fuentes alternativas por Contexto Factual es diferente al uso de fuentes que usualmente se hace en el ejercicio periodístico. Para el chequeo de datos, lo que se busca no es visibilizar distintas posiciones sobre un dato en específico, sino que entregar a la ciudadanía los mejores elementos para evaluar la veracidad de una afirmación.

Para la selección de las fuentes a consultar, se ha conformado una nómina de expertos en diversas temáticas, transversal políticamente, que permite orientar la búsqueda de datos oficiales o alternativas y el análisis en contexto de éstos, tanto de Espacio Público, la Universidad Diego Portales y otras instituciones que pudiesen orientar.

En los artículos de cada chequeo de afirmaciones se consigna el nombre de los expertos consultados en este proceso de verificación. Además, se le pregunta a la fuente si considera que tiene algún tipo de conflicto de interés con el dato o con el candidato que realizó la afirmación en análisis. ')
Setting.create(section:'metodologia', key: 'title_6', value: '6. Análisis de las afirmaciones en contexto')
Setting.create(section:'metodologia', key: 'content_6', value: 'Con el fin de contribuir a una mejor comprensión de la temática y de la afirmación que se está verificando, los datos son puestos en contexto según el tema que se está abordando: comparaciones, datos históricos, antecedentes u otro que pueda ayudar a entregar un mejor entendimiento del chequeo elaborado. ')
Setting.create(section:'metodologia', key: 'title_7', value: '7. Calificación de la veracidad')
Setting.create(section:'metodologia', key: 'content_7', value: ' La calificación de veracidad de las afirmaciones son propuestas por el equipo periodístico de Contexto Factual, que presenta toda la evidencia disponible a través de un artículo al Comité Editorial del proyecto, compuesto por Manuel Arís, María Jaraquemada, Gabriel Ortiz, Francisca Arancibia y Carolina Mosso.

Cada día, dos directores serán quienes califiquen las afirmaciones presentadas por el equipo. Se privilegiarán las decisiones que se adoptan por unanimidad. De no haberla, se solicita al equipo periodístico que aclare o argumente en contra del voto de rechazo a la calificación propuesta.

Las calificaciones posibles son:

Inchequeable

La afirmación se basa en hechos o datos que no existen o no están disponibles para permitir proceder en un chequeo. Contiene ciertas palabras que convierte en la afirmación en algo interpretable.

Verdadero

La afirmación ha demostrado ser verdadera, al ser contrastada con las fuentes, expertos y expertas y otros datos confiables.

Verdadero, pero…

La afirmación es consistente con los datos disponibles, pero no precisa completamente la información entregada.

Discutible

No es claro que la afirmación sea cierta o errónea, pues la conclusión depende de las variables con las que se analice, puede coincidir parcialmente con algunos datos, o bien surge de datos o investigaciones con falta de sustento.

Exagerado

La afirmación otorga proporciones excesivas al argumento al que alude, pero contiene algo de veracidad a modo general en los dichos.

Falso, pero

La afirmación es sustancialmente falsa, pero su formulación se basa en algo veraz.

Falso

La afirmación, contrastada con fuentes y datos serios y confiables, ha demostrado ser falsa.

La nota sobre la verificación es publicada en el sitio web de Contexto Factual, además de ser difundida por las redes sociales de Contexto, Espacio Público y la Universidad Diego Portales y en otros medios de comunicación con los que se hagan alianzas para estos efectos.')
Setting.create(section:'metodologia', key: 'title_8', value: 'Metodología de verificación de la desinformación viral')
Setting.create(section:'metodologia', key: 'content_8', value: ' La información viral será considerada aquellos datos, cifras, afirmaciones, fotos u otras piezas de información que circulen por redes sociales (Twitter, Instagram, Facebook) que tenga un alcance importante. Puede ser cualquier persona común y corriente o en determinadas ocasiones autoridades o personas de la esfera pública. Pueden ser medios de comunicación locales o informales. En caso de ser calificada como falsa, nos referiremos a ella como desinformación viral. En su mayoría serán entregadas por el equipo de Interpreta.

Al igual que en la metodología de verificación de afirmación, el chequeo de desinformación viral constituye una serie de pasos:

1. Seleccionar un contenido sospechoso de las redes sociales monitoreadas. Puede causar sospecha por su alcance o el contenido que está difundiendo, muchas veces para desacreditar personas, instituciones, sectores políticos, entre otros.
2. Ponderar la relevancia de la información que se está entregando y la persona que la emite.
3. Consultar, cuando es identificable, a la fuente original
4. Consultar, cuando es identificable, a las personas involucradas en la información entregada
5. Consultar a la fuente oficial
6. Consultar fuentes alternativas
7. Entregar contexto
8. Calificar

Al igual que en la metodología de verificación de afirmación, el chequeo de desinformación viral constituye una serie de pasos:

Engañoso

La afirmación se basa en datos certeros, pero intencionalmente o no, estos han sido manipulados para generar un mensaje particular.

Verdadero

La afirmación ha demostrado ser verdadera, al ser contrastada con las fuentes, expertos y expertas y otros datos confiables.

Falso

La afirmación, contrastada con fuentes y datos serios y confiables, ha demostrado ser falsa.

Inchequeable

La afirmación se basa en hechos o datos que no existen o no están disponibles para permitir proceder en un chequeo. Contiene ciertas palabras que convierte en la afirmación en algo interpretable. ')
Setting.create(section:'metodologia', key: 'title_9', value: 'Política de correcciones y actualizaciones')
Setting.create(section:'metodologia', key: 'content_9', value: ' Contexto Factual busca mejorar la calidad de la democracia y promover que el debate de los líderes públicos se base en datos confiables e información fidedigna. Para cumplir con este objetivo, hemos establecido una política de actualización y correcciones en el caso de que la publicación nuestra publicación sea inexacta.

En caso de que un lector de aviso sobre un posible error, inexactitud o falta de información en un artículo, el equipo periodístico revisará el contenido y, en el caso que corresponda, publicar la corrección o actualización, consignado en el texto la explicación de corrección o actualización, su motivo y la fecha de modificación.
Para hacer llegar comentarios o nuevos antecedentes sobre los artículos publicados, los lectores pueden escribir a contextofactual@espaciopublico.cl.')

Setting.create(section:'consejo editorial', key: 'consejo_subtitle', value: 'Posible título')
Setting.create(section:'quienes_somos', key: 'colaboradoras', value: 'Contexto también cuenta para su implementación con organizaciones colaboradoras')
Setting.create(section:'quienes_somos', key: 'descarga_text', value: 'Descargar Manual de Expertos')
Setting.create(section:'quienes_somos', key: 'descarga_url', value: 'Descargar Manual de Expertos')
Setting.create(section:'quienes_somos', key: 'organizaciones_text', value: 'Es un proyecto implementado por cuatro organizaciones ...')

Setting.create(section:'carrousel', key: 'slide_1_title', value: 'Noticia')
Setting.create(section:'carrousel', key: 'slide_1_header', value: 'Campaña por un Plebiscito Seguro y Participativo')
Setting.create(section:'carrousel', key: 'slide_1_content', value: 'Más de 200 personas y organizaciones han suscrito a la iniciativa impulsada por Contexto, para promover un Plebiscito Seguro y Participativo.')
Setting.create(section:'carrousel', key: 'slide_1_link', value: 'aaaa')

Setting.create(section:'carrousel', key: 'slide_2_title', value: 'Noticia')
Setting.create(section:'carrousel', key: 'slide_2_header', value: 'Campaña por un Plebiscito Seguro y Participativo')
Setting.create(section:'carrousel', key: 'slide_2_content', value: 'Más de 200 personas y organizaciones han suscrito a la iniciativa impulsada por Contexto, para promover un Plebiscito Seguro y Participativo.')
Setting.create(section:'carrousel', key: 'slide_2_link', value: 'aaaa')

Setting.create(section:'carrousel', key: 'slide_3_title', value: 'Documentos Destacados')
Setting.create(section:'carrousel', key: 'slide_3_header', value: 'Cómo se han redactado las Constituciones en Chile')
Setting.create(section:'carrousel', key: 'slide_3_content', value: 'En este documento se explica el modo en que se hicieron las 10 constituciones que han regido en Chile desde su independencia. Además, se da cuenta del contexto en que surgieron y los mecanismos para escribirla.')
Setting.create(section:'carrousel', key: 'slide_3_link', value: 'aaaa')

Organization.create(description: Faker::Lorem.sentence(4), name: "LabCon", url: "https://www.udp.cl/")
Organization.create(description: Faker::Lorem.sentence(4), name: "Espacio Público", url: "https://www.espaciopublico.cl/")
Organization.create(description: Faker::Lorem.sentence(4), name: "Corporación Humanas", url: "http://www.humanas.cl/")
Organization.create(description: Faker::Lorem.sentence(4), name: "Observatorio Ciudadano", url: "https://observatorio.cl/")

Organization.all.each do |o|
  o.picture.attach(io: File.open(Rails.root + 'app/assets/images/smile.svg'), filename: 'image.svg')
  Member.create(name: Faker::GreekPhilosophers.name, position: Faker::Lorem.sentence(10), organization: o)
  Member.create(name: Faker::GreekPhilosophers.name, position: Faker::Lorem.sentence(10), organization: o)
  Member.create(name: Faker::GreekPhilosophers.name, position: Faker::Lorem.sentence(10), organization: o)
  Member.create(name: Faker::GreekPhilosophers.name, position: Faker::Lorem.sentence(10), organization: o)
end

pc1 = PostCategory.create(name: "Reportajes", description: Faker::Lorem.sentence(3))
pc2 = PostCategory.create(name: "Notas", description: Faker::Lorem.sentence(3))
pc3 = PostCategory.create(name: "Noticias", description: Faker::Lorem.sentence(3))

q1 = Qualification.create(label: "Verdad", name:"verdadero")
q2 = Qualification.create(label: "Inchequeable", name:"inchequeable")
q3 = Qualification.create(label: "Falso", name:"falso")
q4 = Qualification.create(label: "Verdad pero", name:"verdad-pero")
q5 = Qualification.create(label: "Discutible", name:"discutible")
q6 = Qualification.create(label: "Exagerado", name:"exagerado")
q7 = Qualification.create(label: "Falso pero", name:"falso-pero")

Post.create(author: Faker::GreekPhilosophers.name, fecha: Date.today, title: Faker::Lorem.sentence(10), resume: Faker::Lorem.sentence(40), content: Faker::Lorem.sentence(2800), post_category: pc1)
Post.create(author: Faker::GreekPhilosophers.name, fecha: Date.today, title: Faker::Lorem.sentence(10), resume: Faker::Lorem.sentence(40), content: Faker::Lorem.sentence(2850), qualification: q1, post_category: pc1)
Post.create(author: Faker::GreekPhilosophers.name, fecha: Date.today, title: Faker::Lorem.sentence(10), resume: Faker::Lorem.sentence(40), content: Faker::Lorem.sentence(280), qualification: q2, post_category: pc2)
Post.create(author: Faker::GreekPhilosophers.name, fecha: Date.today, title: Faker::Lorem.sentence(10), resume: Faker::Lorem.sentence(40), content: Faker::Lorem.sentence(280), qualification: q3, post_category: pc3)
Post.create(author: Faker::GreekPhilosophers.name, fecha: Date.today, title: Faker::Lorem.sentence(10), resume: Faker::Lorem.sentence(40), content: Faker::Lorem.sentence(2800), post_category: pc2)
Post.create(author: Faker::GreekPhilosophers.name, fecha: Date.today, title: Faker::Lorem.sentence(10), resume: Faker::Lorem.sentence(40), content: Faker::Lorem.sentence(2850), qualification: q1, post_category: pc1)
Post.create(author: Faker::GreekPhilosophers.name, fecha: Date.today, title: Faker::Lorem.sentence(10), resume: Faker::Lorem.sentence(40), content: Faker::Lorem.sentence(280), qualification: q4, post_category: pc2)
Post.create(author: Faker::GreekPhilosophers.name, fecha: Date.today, title: Faker::Lorem.sentence(10), resume: Faker::Lorem.sentence(40), content: Faker::Lorem.sentence(280), qualification: q5, post_category: pc3)

Event.create(name: Faker::Lorem.sentence(5), fecha: Date.today  , lugar: Faker::Restaurant.name, valor: 1000)
Event.create(name: Faker::Lorem.sentence(5), fecha: Date.today+1, lugar: Faker::Restaurant.name, valor: 1000)
Event.create(name: Faker::Lorem.sentence(5), fecha: Date.today+1, lugar: Faker::Restaurant.name, valor: 1000)
Event.create(name: Faker::Lorem.sentence(5), fecha: Date.today+2, lugar: Faker::Restaurant.name, valor: 1000)
Event.create(name: Faker::Lorem.sentence(5), fecha: Date.today+3, lugar: Faker::Restaurant.name, valor: 1000)
Event.create(name: Faker::Lorem.sentence(5), fecha: Date.today+5, lugar: Faker::Restaurant.name, valor: 1000)

4.times do
  cm = CouncilMember.create(name: Faker::GreekPhilosophers.name, especialidad: Faker::Lorem.sentence(2), descripcion: Faker::Lorem.sentence(10))
  cm.picture.attach(io: File.open(Rails.root + 'app/assets/images/podcast.png'), filename: 'image.png')
end
