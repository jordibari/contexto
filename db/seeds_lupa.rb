Circunscripcion.create!([
  {nombre: "I Tarapacá"},
  {nombre: "II Antofagasta"},
  {nombre: "III Atacama"},
  {nombre: "IV Coquimbo"},
  {nombre: "V Valparaíso Cordillera"},
  {nombre: "VI Valparaíso Costa"},
  {nombre: "VII Santiago Poniente"},
  {nombre: "VIII Santiago Oriente"},
  {nombre: "IX O'Higgins"},
  {nombre: "X Maule Norte"},
  {nombre: "XI Maule Sur"},
  {nombre: "XII Biobío Costa"},
  {nombre: "XIII Biobío Cordillera"},
  {nombre: "XIV Araucanía Norte"},
  {nombre: "XV Araucanía Sur"},
  {nombre: "XVI Los Ríos"},
  {nombre: "XVII Los Lagos"},
  {nombre: "XVIII Aysén"},
  {nombre: "XIX Magallanes"}
])

Distrito.create!([
  {circunscripcion_id: 1, nombre: "1", limite_gasto: 10000000},
  {circunscripcion_id: 2, nombre: "2", limite_gasto: 10000000},
  {circunscripcion_id: 3, nombre: "3", limite_gasto: 10000000},
  {circunscripcion_id: 4, nombre: "4", limite_gasto: 10000000},
  {circunscripcion_id: 5, nombre: "5", limite_gasto: 10000000},
  {circunscripcion_id: 6, nombre: "6", limite_gasto: 10000000},
  {circunscripcion_id: 6, nombre: "7", limite_gasto: 10000000},
  {circunscripcion_id: 7, nombre: "8", limite_gasto: 10000000},
  {circunscripcion_id: 7, nombre: "9", limite_gasto: 10000000},
  {circunscripcion_id: 7, nombre: "10", limite_gasto: 10000000},
  {circunscripcion_id: 7, nombre: "11", limite_gasto: 10000000},
  {circunscripcion_id: 7, nombre: "12", limite_gasto: 10000000},
  {circunscripcion_id: 7, nombre: "13", limite_gasto: 10000000},
  {circunscripcion_id: 7, nombre: "14", limite_gasto: 10000000},
  {circunscripcion_id: 8, nombre: "15", limite_gasto: 10000000},
  {circunscripcion_id: 8, nombre: "16", limite_gasto: 10000000},
  {circunscripcion_id: 9, nombre: "17", limite_gasto: 10000000},
  {circunscripcion_id: 9, nombre: "18", limite_gasto: 10000000},
  {circunscripcion_id: 10, nombre: "19", limite_gasto: 10000000},
  {circunscripcion_id: 10, nombre: "20", limite_gasto: 10000000},
  {circunscripcion_id: 10, nombre: "21", limite_gasto: 10000000},
  {circunscripcion_id: 11, nombre: "22", limite_gasto: 10000000},
  {circunscripcion_id: 11, nombre: "23", limite_gasto: 10000000},
  {circunscripcion_id: 12, nombre: "24", limite_gasto: 10000000},
  {circunscripcion_id: 13, nombre: "25", limite_gasto: 10000000},
  {circunscripcion_id: 13, nombre: "26", limite_gasto: 10000000},
  {circunscripcion_id: 14, nombre: "27", limite_gasto: 10000000},
  {circunscripcion_id: 15, nombre: "28", limite_gasto: 10000000}
])

Region.create!([
  {nombre: "Arica y Parinacota", numero: 15, num_romano: "XV"},
  {nombre: "Tarapacá", numero: 1, num_romano: "I"},
  {nombre: "Antofagasta", numero: 2, num_romano: "II"},
  {nombre: "Atacama", numero: 3, num_romano: "III"},
  {nombre: "Coquimbo", numero: 4, num_romano: "IV|"},
  {nombre: "Valparaiso", numero: 5, num_romano: "V"},
  {nombre: "Metropolitana de Santiago", numero: 13, num_romano: "XIII"},
  {nombre: "Libertador General Bernardo O'Higgins", numero: 6, num_romano: "XI"},
  {nombre: "Maule", numero: 7, num_romano: "VII"},
  {nombre: "Biobío", numero: 8, num_romano: "VIII"},
  {nombre: "La Araucanía", numero: 9, num_romano: "IX"},
  {nombre: "Los Ríos", numero: 14, num_romano: "XIV"},
  {nombre: "Los Lagos", numero: 10, num_romano: "X"},
  {nombre: "Aisén del General Carlos Ibáñez del Campo", numero: 11, num_romano: "XI"},
  {nombre: "Magallanes y de la Antártica Chilena", numero: 12, num_romano: "XII"}
])

Provincia.create!([
  {nombre: "Arica", region_id: 1},
  {nombre: "Parinacota", region_id: 1},
  {nombre: "Iquique", region_id: 2},
  {nombre: "El Tamarugal", region_id: 2},
  {nombre: "Antofagasta", region_id: 3},
  {nombre: "El Loa", region_id: 3},
  {nombre: "Tocopilla", region_id: 3},
  {nombre: "Chañaral", region_id: 4},
  {nombre: "Copiapó", region_id: 4},
  {nombre: "Huasco", region_id: 4},
  {nombre: "Choapa", region_id: 5},
  {nombre: "Elqui", region_id: 5},
  {nombre: "Limarí", region_id: 5},
  {nombre: "Isla de Pascua", region_id: 6},
  {nombre: "Los Andes", region_id: 6},
  {nombre: "Petorca", region_id: 6},
  {nombre: "Quillota", region_id: 6},
  {nombre: "San Antonio", region_id: 6},
  {nombre: "San Felipe de Aconcagua", region_id: 6},
  {nombre: "Valparaiso", region_id: 6},
  {nombre: "Chacabuco", region_id: 7},
  {nombre: "Cordillera", region_id: 7},
  {nombre: "Maipo", region_id: 7},
  {nombre: "Melipilla", region_id: 7},
  {nombre: "Santiago", region_id: 7},
  {nombre: "Talagante", region_id: 7},
  {nombre: "Cachapoal", region_id: 8},
  {nombre: "Cardenal Caro", region_id: 8},
  {nombre: "Colchagua", region_id: 8},
  {nombre: "Cauquenes", region_id: 9},
  {nombre: "Curicó", region_id: 9},
  {nombre: "Linares", region_id: 9},
  {nombre: "Talca", region_id: 9},
  {nombre: "Arauco", region_id: 10},
  {nombre: "Bio Bío", region_id: 10},
  {nombre: "Concepción", region_id: 10},
  {nombre: "Ñuble", region_id: 10},
  {nombre: "Cautín", region_id: 11},
  {nombre: "Malleco", region_id: 11},
  {nombre: "Valdivia", region_id: 12},
  {nombre: "Ranco", region_id: 12},
  {nombre: "Chiloé", region_id: 13},
  {nombre: "Llanquihue", region_id: 13},
  {nombre: "Osorno", region_id: 13},
  {nombre: "Palena", region_id: 13},
  {nombre: "Aisén", region_id: 14},
  {nombre: "Capitán Prat", region_id: 14},
  {nombre: "Coihaique", region_id: 14},
  {nombre: "General Carrera", region_id: 14},
  {nombre: "Antártica Chilena", region_id: 15},
  {nombre: "Magallanes", region_id: 15},
  {nombre: "Tierra del Fuego", region_id: 15},
  {nombre: "Última Esperanza", region_id: 15}
])

Comuna.create!([
  {distrito_id: 1, provincia_id: 1, nombre: "Arica"},
  {distrito_id: 1, provincia_id: 1, nombre: "Camarones"},
  {distrito_id: 1, provincia_id: 2, nombre: "General Lagos"},
  {distrito_id: 1, provincia_id: 2, nombre: "Putre"},
  {distrito_id: 2, provincia_id: 3, nombre: "Alto Hospicio"},
  {distrito_id: 2, provincia_id: 3, nombre: "Iquique"},
  {distrito_id: 2, provincia_id: 4, nombre: "Camiña"},
  {distrito_id: 2, provincia_id: 4, nombre: "Colchane"},
  {distrito_id: 2, provincia_id: 4, nombre: "Huara"},
  {distrito_id: 2, provincia_id: 4, nombre: "Pica"},
  {distrito_id: 2, provincia_id: 4, nombre: "Pozo Almonte"},
  {distrito_id: 3, provincia_id: 5, nombre: "Antofagasta"},
  {distrito_id: 3, provincia_id: 5, nombre: "Mejillones"},
  {distrito_id: 3, provincia_id: 5, nombre: "Sierra Gorda"},
  {distrito_id: 3, provincia_id: 5, nombre: "Taltal"},
  {distrito_id: 3, provincia_id: 6, nombre: "Calama"},
  {distrito_id: 3, provincia_id: 6, nombre: "Ollague"},
  {distrito_id: 3, provincia_id: 6, nombre: "San Pedro de Atacama"},
  {distrito_id: 3, provincia_id: 7, nombre: "María Elena"},
  {distrito_id: 3, provincia_id: 7, nombre: "Tocopilla"},
  {distrito_id: 4, provincia_id: 8, nombre: "Chañaral"},
  {distrito_id: 4, provincia_id: 8, nombre: "Diego de Almagro"},
  {distrito_id: 4, provincia_id: 9, nombre: "Caldera"},
  {distrito_id: 4, provincia_id: 9, nombre: "Copiapó"},
  {distrito_id: 4, provincia_id: 9, nombre: "Tierra Amarilla"},
  {distrito_id: 4, provincia_id: 10, nombre: "Alto del Carmen"},
  {distrito_id: 4, provincia_id: 10, nombre: "Freirina"},
  {distrito_id: 4, provincia_id: 10, nombre: "Huasco"},
  {distrito_id: 4, provincia_id: 10, nombre: "Vallenar"},
  {distrito_id: 5, provincia_id: 11, nombre: "Canela"},
  {distrito_id: 5, provincia_id: 11, nombre: "Illapel"},
  {distrito_id: 5, provincia_id: 11, nombre: "Los Vilos"},
  {distrito_id: 5, provincia_id: 11, nombre: "Salamanca"},
  {distrito_id: 5, provincia_id: 12, nombre: "Andacollo"},
  {distrito_id: 5, provincia_id: 12, nombre: "Coquimbo"},
  {distrito_id: 5, provincia_id: 12, nombre: "La Higuera"},
  {distrito_id: 5, provincia_id: 12, nombre: "La Serena"},
  {distrito_id: 5, provincia_id: 12, nombre: "Paihuaco"},
  {distrito_id: 5, provincia_id: 12, nombre: "Vicuña"},
  {distrito_id: 5, provincia_id: 13, nombre: "Combarbalá"},
  {distrito_id: 5, provincia_id: 13, nombre: "Monte Patria"},
  {distrito_id: 5, provincia_id: 13, nombre: "Ovalle"},
  {distrito_id: 5, provincia_id: 13, nombre: "Punitaqui"},
  {distrito_id: 5, provincia_id: 13, nombre: "Río Hurtado"},
  {distrito_id: 7, provincia_id: 14, nombre: "Isla de Pascua"},
  {distrito_id: 6, provincia_id: 15, nombre: "Calle Larga"},
  {distrito_id: 6, provincia_id: 15, nombre: "Los Andes"},
  {distrito_id: 6, provincia_id: 15, nombre: "Rinconada"},
  {distrito_id: 6, provincia_id: 15, nombre: "San Esteban"},
  {distrito_id: 6, provincia_id: 16, nombre: "La Ligua"},
  {distrito_id: 6, provincia_id: 16, nombre: "Papudo"},
  {distrito_id: 6, provincia_id: 16, nombre: "Petorca"},
  {distrito_id: 6, provincia_id: 16, nombre: "Zapallar"},
  {distrito_id: 6, provincia_id: 17, nombre: "Hijuelas"},
  {distrito_id: 6, provincia_id: 17, nombre: "La Calera"},
  {distrito_id: 6, provincia_id: 17, nombre: "La Cruz"},
  {distrito_id: 6, provincia_id: 17, nombre: "Limache"},
  {distrito_id: 6, provincia_id: 17, nombre: "Nogales"},
  {distrito_id: 6, provincia_id: 17, nombre: "Olmué"},
  {distrito_id: 6, provincia_id: 17, nombre: "Quillota"},
  {distrito_id: 6, provincia_id: 18, nombre: "Algarrobo"},
  {distrito_id: 6, provincia_id: 18, nombre: "Cartagena"},
  {distrito_id: 6, provincia_id: 18, nombre: "El Quisco"},
  {distrito_id: 6, provincia_id: 18, nombre: "El Tabo"},
  {distrito_id: 6, provincia_id: 18, nombre: "San Antonio"},
  {distrito_id: 6, provincia_id: 18, nombre: "Santo Domingo"},
  {distrito_id: 6, provincia_id: 19, nombre: "Catemu"},
  {distrito_id: 6, provincia_id: 19, nombre: "Llaillay"},
  {distrito_id: 6, provincia_id: 19, nombre: "Panquehue"},
  {distrito_id: 6, provincia_id: 19, nombre: "Putaendo"},
  {distrito_id: 6, provincia_id: 19, nombre: "San Felipe"},
  {distrito_id: 6, provincia_id: 19, nombre: "Santa María"},
  {distrito_id: 7, provincia_id: 20, nombre: "Casablanca"},
  {distrito_id: 7, provincia_id: 20, nombre: "Concón"},
  {distrito_id: 7, provincia_id: 20, nombre: "Juan Fernández"},
  {distrito_id: 7, provincia_id: 20, nombre: "Puchuncaví"},
  {distrito_id: 7, provincia_id: 20, nombre: "Quilpué"},
  {distrito_id: 7, provincia_id: 20, nombre: "Quintero"},
  {distrito_id: 7, provincia_id: 20, nombre: "Valparaíso"},
  {distrito_id: 6, provincia_id: 20, nombre: "Villa Alemana"},
  {distrito_id: 7, provincia_id: 20, nombre: "Viña del Mar"},
  {distrito_id: 8, provincia_id: 21, nombre: "Colina"},
  {distrito_id: 8, provincia_id: 21, nombre: "Lampa"},
  {distrito_id: 8, provincia_id: 21, nombre: "Tiltil"},
  {distrito_id: 8, provincia_id: 22, nombre: "Pirque"},
  {distrito_id: 8, provincia_id: 22, nombre: "Puente Alto"},
  {distrito_id: 8, provincia_id: 22, nombre: "San José de Maipo"},
  {distrito_id: 8, provincia_id: 23, nombre: "Buin"},
  {distrito_id: 8, provincia_id: 23, nombre: "Calera de Tango"},
  {distrito_id: 8, provincia_id: 23, nombre: "Paine"},
  {distrito_id: 8, provincia_id: 23, nombre: "San Bernardo"},
  {distrito_id: 8, provincia_id: 24, nombre: "Alhué"},
  {distrito_id: 8, provincia_id: 24, nombre: "Curacaví"},
  {distrito_id: 8, provincia_id: 24, nombre: "María Pinto"},
  {distrito_id: 8, provincia_id: 24, nombre: "Melipilla"},
  {distrito_id: 9, provincia_id: 24, nombre: "San Pedro"},
  {distrito_id: 9, provincia_id: 25, nombre: "Cerrillos"},
  {distrito_id: 9, provincia_id: 25, nombre: "Cerro Navia"},
  {distrito_id: 9, provincia_id: 25, nombre: "Conchalí"},
  {distrito_id: 9, provincia_id: 25, nombre: "El Bosque"},
  {distrito_id: 9, provincia_id: 25, nombre: "Estación Central"},
  {distrito_id: 9, provincia_id: 25, nombre: "Huechuraba"},
  {distrito_id: 9, provincia_id: 25, nombre: "Independencia"},
  {distrito_id: 10, provincia_id: 25, nombre: "La Cisterna"},
  {distrito_id: 10, provincia_id: 25, nombre: "La Granja"},
  {distrito_id: 10, provincia_id: 25, nombre: "La Florida"},
  {distrito_id: 10, provincia_id: 25, nombre: "La Pintana"},
  {distrito_id: 10, provincia_id: 25, nombre: "La Reina"},
  {distrito_id: 10, provincia_id: 25, nombre: "Las Condes"},
  {distrito_id: 10, provincia_id: 25, nombre: "Lo Barnechea"},
  {distrito_id: 10, provincia_id: 25, nombre: "Lo Espejo"},
  {distrito_id: 11, provincia_id: 25, nombre: "Lo Prado"},
  {distrito_id: 11, provincia_id: 25, nombre: "Macul"},
  {distrito_id: 11, provincia_id: 25, nombre: "Maipú"},
  {distrito_id: 11, provincia_id: 25, nombre: "Ñuñoa"},
  {distrito_id: 11, provincia_id: 25, nombre: "Pedro Aguirre Cerda"},
  {distrito_id: 11, provincia_id: 25, nombre: "Peñalolén"},
  {distrito_id: 11, provincia_id: 25, nombre: "Providencia"},
  {distrito_id: 11, provincia_id: 25, nombre: "Pudahuel"},
  {distrito_id: 12, provincia_id: 25, nombre: "Quilicura"},
  {distrito_id: 12, provincia_id: 25, nombre: "Quinta Normal"},
  {distrito_id: 12, provincia_id: 25, nombre: "Recoleta"},
  {distrito_id: 12, provincia_id: 25, nombre: "Renca"},
  {distrito_id: 12, provincia_id: 25, nombre: "San Miguel"},
  {distrito_id: 12, provincia_id: 25, nombre: "San Joaquín"},
  {distrito_id: 12, provincia_id: 25, nombre: "San Ramón"},
  {distrito_id: 12, provincia_id: 25, nombre: "Santiago"},
  {distrito_id: 13, provincia_id: 25, nombre: "Vitacura"},
  {distrito_id: 13, provincia_id: 26, nombre: "El Monte"},
  {distrito_id: 13, provincia_id: 26, nombre: "Isla de Maipo"},
  {distrito_id: 13, provincia_id: 26, nombre: "Padre Hurtado"},
  {distrito_id: 13, provincia_id: 26, nombre: "Peñaflor"},
  {distrito_id: 13, provincia_id: 26, nombre: "Talagante"},
  {distrito_id: 13, provincia_id: 27, nombre: "Codegua"},
  {distrito_id: 13, provincia_id: 27, nombre: "Coínco"},
  {distrito_id: 14, provincia_id: 27, nombre: "Coltauco"},
  {distrito_id: 14, provincia_id: 27, nombre: "Doñihue"},
  {distrito_id: 14, provincia_id: 27, nombre: "Graneros"},
  {distrito_id: 14, provincia_id: 27, nombre: "Las Cabras"},
  {distrito_id: 14, provincia_id: 27, nombre: "Machalí"},
  {distrito_id: 14, provincia_id: 27, nombre: "Malloa"},
  {distrito_id: 14, provincia_id: 27, nombre: "Mostazal"},
  {distrito_id: 14, provincia_id: 27, nombre: "Olivar"},
  {distrito_id: 15, provincia_id: 27, nombre: "Peumo"},
  {distrito_id: 15, provincia_id: 27, nombre: "Pichidegua"},
  {distrito_id: 15, provincia_id: 27, nombre: "Quinta de Tilcoco"},
  {distrito_id: 15, provincia_id: 27, nombre: "Rancagua"},
  {distrito_id: 15, provincia_id: 27, nombre: "Rengo"},
  {distrito_id: 15, provincia_id: 27, nombre: "Requínoa"},
  {distrito_id: 15, provincia_id: 27, nombre: "San Vicente de Tagua Tagua"},
  {distrito_id: 15, provincia_id: 28, nombre: "La Estrella"},
  {distrito_id: 16, provincia_id: 28, nombre: "Litueche"},
  {distrito_id: 16, provincia_id: 28, nombre: "Marchihue"},
  {distrito_id: 16, provincia_id: 28, nombre: "Navidad"},
  {distrito_id: 16, provincia_id: 28, nombre: "Peredones"},
  {distrito_id: 16, provincia_id: 28, nombre: "Pichilemu"},
  {distrito_id: 16, provincia_id: 29, nombre: "Chépica"},
  {distrito_id: 16, provincia_id: 29, nombre: "Chimbarongo"},
  {distrito_id: 16, provincia_id: 29, nombre: "Lolol"},
  {distrito_id: 16, provincia_id: 29, nombre: "Nancagua"},
  {distrito_id: 16, provincia_id: 29, nombre: "Palmilla"},
  {distrito_id: 16, provincia_id: 29, nombre: "Peralillo"},
  {distrito_id: 16, provincia_id: 29, nombre: "Placilla"},
  {distrito_id: 16, provincia_id: 29, nombre: "Pumanque"},
  {distrito_id: 16, provincia_id: 29, nombre: "San Fernando"},
  {distrito_id: 16, provincia_id: 29, nombre: "Santa Cruz"},
  {distrito_id: 16, provincia_id: 30, nombre: "Cauquenes"},
  {distrito_id: 16, provincia_id: 30, nombre: "Chanco"},
  {distrito_id: 16, provincia_id: 30, nombre: "Pelluhue"},
  {distrito_id: 16, provincia_id: 31, nombre: "Curicó"},
  {distrito_id: 16, provincia_id: 31, nombre: "Hualañé"},
  {distrito_id: 17, provincia_id: 31, nombre: "Licantén"},
  {distrito_id: 17, provincia_id: 31, nombre: "Molina"},
  {distrito_id: 17, provincia_id: 31, nombre: "Rauco"},
  {distrito_id: 17, provincia_id: 31, nombre: "Romeral"},
  {distrito_id: 17, provincia_id: 31, nombre: "Sagrada Familia"},
  {distrito_id: 17, provincia_id: 31, nombre: "Teno"},
  {distrito_id: 17, provincia_id: 31, nombre: "Vichuquén"},
  {distrito_id: 17, provincia_id: 32, nombre: "Colbún"},
  {distrito_id: 17, provincia_id: 32, nombre: "Linares"},
  {distrito_id: 17, provincia_id: 32, nombre: "Longaví"},
  {distrito_id: 17, provincia_id: 32, nombre: "Parral"},
  {distrito_id: 17, provincia_id: 32, nombre: "Retiro"},
  {distrito_id: 17, provincia_id: 32, nombre: "San Javier"},
  {distrito_id: 17, provincia_id: 32, nombre: "Villa Alegre"},
  {distrito_id: 17, provincia_id: 32, nombre: "Yerbas Buenas"},
  {distrito_id: 17, provincia_id: 33, nombre: "Constitución"},
  {distrito_id: 17, provincia_id: 33, nombre: "Curepto"},
  {distrito_id: 17, provincia_id: 33, nombre: "Empedrado"},
  {distrito_id: 17, provincia_id: 33, nombre: "Maule"},
  {distrito_id: 17, provincia_id: 33, nombre: "Pelarco"},
  {distrito_id: 18, provincia_id: 33, nombre: "Pencahue"},
  {distrito_id: 18, provincia_id: 33, nombre: "Río Claro"},
  {distrito_id: 18, provincia_id: 33, nombre: "San Clemente"},
  {distrito_id: 18, provincia_id: 33, nombre: "San Rafael"},
  {distrito_id: 18, provincia_id: 33, nombre: "Talca"},
  {distrito_id: 18, provincia_id: 34, nombre: "Arauco"},
  {distrito_id: 18, provincia_id: 34, nombre: "Cañete"},
  {distrito_id: 18, provincia_id: 34, nombre: "Contulmo"},
  {distrito_id: 18, provincia_id: 34, nombre: "Curanilahue"},
  {distrito_id: 18, provincia_id: 34, nombre: "Lebu"},
  {distrito_id: 18, provincia_id: 34, nombre: "Los Álamos"},
  {distrito_id: 18, provincia_id: 34, nombre: "Tirúa"},
  {distrito_id: 18, provincia_id: 35, nombre: "Alto Biobío"},
  {distrito_id: 18, provincia_id: 35, nombre: "Antuco"},
  {distrito_id: 18, provincia_id: 35, nombre: "Cabrero"},
  {distrito_id: 18, provincia_id: 35, nombre: "Laja"},
  {distrito_id: 18, provincia_id: 35, nombre: "Los Ángeles"},
  {distrito_id: 18, provincia_id: 35, nombre: "Mulchén"},
  {distrito_id: 18, provincia_id: 35, nombre: "Nacimiento"},
  {distrito_id: 18, provincia_id: 35, nombre: "Negrete"},
  {distrito_id: 19, provincia_id: 35, nombre: "Quilaco"},
  {distrito_id: 19, provincia_id: 35, nombre: "Quilleco"},
  {distrito_id: 19, provincia_id: 35, nombre: "San Rosendo"},
  {distrito_id: 19, provincia_id: 35, nombre: "Santa Bárbara"},
  {distrito_id: 19, provincia_id: 35, nombre: "Tucapel"},
  {distrito_id: 19, provincia_id: 35, nombre: "Yumbel"},
  {distrito_id: 19, provincia_id: 36, nombre: "Chiguayante"},
  {distrito_id: 19, provincia_id: 36, nombre: "Concepción"},
  {distrito_id: 19, provincia_id: 36, nombre: "Coronel"},
  {distrito_id: 19, provincia_id: 36, nombre: "Florida"},
  {distrito_id: 19, provincia_id: 36, nombre: "Hualpén"},
  {distrito_id: 19, provincia_id: 36, nombre: "Hualqui"},
  {distrito_id: 19, provincia_id: 36, nombre: "Lota"},
  {distrito_id: 19, provincia_id: 36, nombre: "Penco"},
  {distrito_id: 19, provincia_id: 36, nombre: "San Pedro de La Paz"},
  {distrito_id: 19, provincia_id: 36, nombre: "Santa Juana"},
  {distrito_id: 19, provincia_id: 36, nombre: "Talcahuano"},
  {distrito_id: 19, provincia_id: 36, nombre: "Tomé"},
  {distrito_id: 19, provincia_id: 37, nombre: "Bulnes"},
  {distrito_id: 19, provincia_id: 37, nombre: "Chillán"},
  {distrito_id: 20, provincia_id: 37, nombre: "Chillán Viejo"},
  {distrito_id: 20, provincia_id: 37, nombre: "Cobquecura"},
  {distrito_id: 20, provincia_id: 37, nombre: "Coelemu"},
  {distrito_id: 20, provincia_id: 37, nombre: "Coihueco"},
  {distrito_id: 20, provincia_id: 37, nombre: "El Carmen"},
  {distrito_id: 20, provincia_id: 37, nombre: "Ninhue"},
  {distrito_id: 20, provincia_id: 37, nombre: "Ñiquen"},
  {distrito_id: 20, provincia_id: 37, nombre: "Pemuco"},
  {distrito_id: 21, provincia_id: 37, nombre: "Pinto"},
  {distrito_id: 21, provincia_id: 37, nombre: "Portezuelo"},
  {distrito_id: 21, provincia_id: 37, nombre: "Quillón"},
  {distrito_id: 21, provincia_id: 37, nombre: "Quirihue"},
  {distrito_id: 21, provincia_id: 37, nombre: "Ránquil"},
  {distrito_id: 21, provincia_id: 37, nombre: "San Carlos"},
  {distrito_id: 21, provincia_id: 37, nombre: "San Fabián"},
  {distrito_id: 21, provincia_id: 37, nombre: "San Ignacio"},
  {distrito_id: 22, provincia_id: 37, nombre: "San Nicolás"},
  {distrito_id: 22, provincia_id: 37, nombre: "Treguaco"},
  {distrito_id: 22, provincia_id: 37, nombre: "Yungay"},
  {distrito_id: 22, provincia_id: 38, nombre: "Carahue"},
  {distrito_id: 22, provincia_id: 38, nombre: "Cholchol"},
  {distrito_id: 22, provincia_id: 38, nombre: "Cunco"},
  {distrito_id: 22, provincia_id: 38, nombre: "Curarrehue"},
  {distrito_id: 22, provincia_id: 38, nombre: "Freire"},
  {distrito_id: 23, provincia_id: 38, nombre: "Galvarino"},
  {distrito_id: 23, provincia_id: 38, nombre: "Gorbea"},
  {distrito_id: 23, provincia_id: 38, nombre: "Lautaro"},
  {distrito_id: 23, provincia_id: 38, nombre: "Loncoche"},
  {distrito_id: 23, provincia_id: 38, nombre: "Melipeuco"},
  {distrito_id: 23, provincia_id: 38, nombre: "Nueva Imperial"},
  {distrito_id: 23, provincia_id: 38, nombre: "Padre Las Casas"},
  {distrito_id: 23, provincia_id: 38, nombre: "Perquenco"},
  {distrito_id: 24, provincia_id: 38, nombre: "Pitrufquén"},
  {distrito_id: 24, provincia_id: 38, nombre: "Pucón"},
  {distrito_id: 24, provincia_id: 38, nombre: "Saavedra"},
  {distrito_id: 24, provincia_id: 38, nombre: "Temuco"},
  {distrito_id: 24, provincia_id: 38, nombre: "Teodoro Schmidt"},
  {distrito_id: 24, provincia_id: 38, nombre: "Toltén"},
  {distrito_id: 24, provincia_id: 38, nombre: "Vilcún"},
  {distrito_id: 24, provincia_id: 38, nombre: "Villarrica"},
  {distrito_id: 25, provincia_id: 39, nombre: "Angol"},
  {distrito_id: 25, provincia_id: 39, nombre: "Collipulli"},
  {distrito_id: 25, provincia_id: 39, nombre: "Curacautín"},
  {distrito_id: 25, provincia_id: 39, nombre: "Ercilla"},
  {distrito_id: 25, provincia_id: 39, nombre: "Lonquimay"},
  {distrito_id: 25, provincia_id: 39, nombre: "Los Sauces"},
  {distrito_id: 25, provincia_id: 39, nombre: "Lumaco"},
  {distrito_id: 25, provincia_id: 39, nombre: "Purén"},
  {distrito_id: 25, provincia_id: 39, nombre: "Renaico"},
  {distrito_id: 25, provincia_id: 39, nombre: "Traiguén"},
  {distrito_id: 25, provincia_id: 39, nombre: "Victoria"},
  {distrito_id: 25, provincia_id: 40, nombre: "Corral"},
  {distrito_id: 25, provincia_id: 40, nombre: "Lanco"},
  {distrito_id: 25, provincia_id: 40, nombre: "Los Lagos"},
  {distrito_id: 25, provincia_id: 40, nombre: "Máfil"},
  {distrito_id: 25, provincia_id: 40, nombre: "Mariquina"},
  {distrito_id: 25, provincia_id: 40, nombre: "Paillaco"},
  {distrito_id: 25, provincia_id: 40, nombre: "Panguipulli"},
  {distrito_id: 25, provincia_id: 40, nombre: "Valdivia"},
  {distrito_id: 25, provincia_id: 41, nombre: "Futrono"},
  {distrito_id: 25, provincia_id: 41, nombre: "La Unión"},
  {distrito_id: 25, provincia_id: 41, nombre: "Lago Ranco"},
  {distrito_id: 25, provincia_id: 41, nombre: "Río Bueno"},
  {distrito_id: 25, provincia_id: 42, nombre: "Ancud"},
  {distrito_id: 25, provincia_id: 42, nombre: "Castro"},
  {distrito_id: 25, provincia_id: 42, nombre: "Chonchi"},
  {distrito_id: 25, provincia_id: 42, nombre: "Curaco de Vélez"},
  {distrito_id: 25, provincia_id: 42, nombre: "Dalcahue"},
  {distrito_id: 25, provincia_id: 42, nombre: "Puqueldón"},
  {distrito_id: 25, provincia_id: 42, nombre: "Queilén"},
  {distrito_id: 25, provincia_id: 42, nombre: "Quemchi"},
  {distrito_id: 25, provincia_id: 42, nombre: "Quellón"},
  {distrito_id: 25, provincia_id: 42, nombre: "Quinchao"},
  {distrito_id: 25, provincia_id: 43, nombre: "Calbuco"},
  {distrito_id: 25, provincia_id: 43, nombre: "Cochamó"},
  {distrito_id: 25, provincia_id: 43, nombre: "Fresia"},
  {distrito_id: 25, provincia_id: 43, nombre: "Frutillar"},
  {distrito_id: 25, provincia_id: 43, nombre: "Llanquihue"},
  {distrito_id: 25, provincia_id: 43, nombre: "Los Muermos"},
  {distrito_id: 26, provincia_id: 43, nombre: "Maullín"},
  {distrito_id: 26, provincia_id: 43, nombre: "Puerto Montt"},
  {distrito_id: 26, provincia_id: 43, nombre: "Puerto Varas"},
  {distrito_id: 26, provincia_id: 44, nombre: "Osorno"},
  {distrito_id: 26, provincia_id: 44, nombre: "Puero Octay"},
  {distrito_id: 26, provincia_id: 44, nombre: "Purranque"},
  {distrito_id: 26, provincia_id: 44, nombre: "Puyehue"},
  {distrito_id: 26, provincia_id: 44, nombre: "Río Negro"},
  {distrito_id: 26, provincia_id: 44, nombre: "San Juan de la Costa"},
  {distrito_id: 27, provincia_id: 44, nombre: "San Pablo"},
  {distrito_id: 27, provincia_id: 45, nombre: "Chaitén"},
  {distrito_id: 27, provincia_id: 45, nombre: "Futaleufú"},
  {distrito_id: 27, provincia_id: 45, nombre: "Hualaihué"},
  {distrito_id: 27, provincia_id: 45, nombre: "Palena"},
  {distrito_id: 27, provincia_id: 46, nombre: "Aisén"},
  {distrito_id: 27, provincia_id: 46, nombre: "Cisnes"},
  {distrito_id: 27, provincia_id: 46, nombre: "Guaitecas"},
  {distrito_id: 27, provincia_id: 47, nombre: "Cochrane"},
  {distrito_id: 28, provincia_id: 47, nombre: "O'higgins"},
  {distrito_id: 28, provincia_id: 47, nombre: "Tortel"},
  {distrito_id: 28, provincia_id: 48, nombre: "Coihaique"},
  {distrito_id: 28, provincia_id: 48, nombre: "Lago Verde"},
  {distrito_id: 28, provincia_id: 49, nombre: "Chile Chico"},
  {distrito_id: 28, provincia_id: 49, nombre: "Río Ibáñez"},
  {distrito_id: 28, provincia_id: 50, nombre: "Antártica"},
  {distrito_id: 28, provincia_id: 50, nombre: "Cabo de Hornos"},
  {distrito_id: 28, provincia_id: 51, nombre: "Laguna Blanca"},
  {distrito_id: 28, provincia_id: 51, nombre: "Punta Arenas"},
  {distrito_id: 28, provincia_id: 51, nombre: "Río Verde"},
  {distrito_id: 28, provincia_id: 51, nombre: "San Gregorio"},
  {distrito_id: 28, provincia_id: 52, nombre: "Porvenir"},
  {distrito_id: 28, provincia_id: 52, nombre: "Primavera"},
  {distrito_id: 28, provincia_id: 52, nombre: "Timaukel"},
  {distrito_id: 28, provincia_id: 53, nombre: "Natales"},
  {distrito_id: 28, provincia_id: 53, nombre: "Torres del Paine"}
])

coords = [
  {
  "name": "Santiago",
  "lng": "-70.6666667",
  "lat": "-33.4500000"
}, {
  "name": "Cerro Navia",
  "lng": "-70.7166667",
  "lat": "-33.4166667"
}, {
  "name": "El Bosque",
  "lng": "-70.7000000",
  "lat": "-33.5666667"
}, {
  "name": "Huechuraba",
  "lng": "-70.6666667",
  "lat": "-33.3500000"
}, {
  "name": "La Cisterna",
  "lng": "-70.6833333",
  "lat": "-33.5500000"
}, {
  "name": "La Granja",
  "lng": "-70.5833333",
  "lat": "-33.5833333"
}, {
  "name": "La Reina",
  "lng": "-70.5500000",
  "lat": "-33.4500000"
}, {
  "name": "Lo Barnechea",
  "lng": "-70.5166667",
  "lat": "-33.3500000"
}, {
  "name": "Lo Prado",
  "lng": "-70.7166667",
  "lat": "-33.4333333"
}, {
  "name": "Maip\u00fa",
  "lng": "-70.7666667",
  "lat": "-33.5166667"
}, {
  "name": "Pedro Aguirre Cerda",
  "lng": "-70.6780860",
  "lat": "-33.4924550"
}, {
  "name": "Providencia",
  "lng": "-70.6166667",
  "lat": "-33.4333333"
}, {
  "name": "Quilicura",
  "lng": "-70.7500000",
  "lat": "-33.3666667"
}, {
  "name": "Recoleta",
  "lng": "-33.4081480",
  "lat": "-70.6391920"
}, {
  "name": "San Joaqu\u00edn",
  "lng": "-70.6166667",
  "lat": "-33.5000000"
}, {
  "name": "San Ram\u00f3n",
  "lng": "-70.5000000",
  "lat": "-33.4500000"
}, {
  "name": "Puente Alto",
  "lng": "-70.5833333",
  "lat": "-33.6166667"
}, {
  "name": "Padre Hurtado",
  "lng": "-70.8333333",
  "lat": "-33.5666667"
}, {
  "name": "El Monte",
  "lng": "-71.0166667",
  "lat": "-33.6833333"
}, {
  "name": "San Pedro",
  "lng": "-71.4666667",
  "lat": "-33.9000000"
}, {
  "name": "Curacav\u00ed",
  "lng": "-71.1500000",
  "lat": "-33.4000000"
}, {
  "name": "Melipilla",
  "lng": "-71.2166667",
  "lat": "-33.7000000"
}, {
  "name": "Calera de Tango",
  "lng": "-70.8166667",
  "lat": "-33.6500000"
}, {
  "name": "San Bernardo",
  "lng": "-70.7166667",
  "lat": "-33.6000000"
}, {
  "name": "Lampa",
  "lng": "-70.9000000",
  "lat": "-33.2833333"
}, {
  "name": "San Jos\u00e9 de Maipo",
  "lng": "-70.3666667",
  "lat": "-33.6333333"
}, {
  "name": "Pe\u00f1aflor",
  "lng": "-70.9166667",
  "lat": "-33.6166667"
}, {
  "name": "Isla de Maipo",
  "lng": "-70.9000000",
  "lat": "-33.7500000"
}, {
  "name": "Talagante",
  "lng": "-70.9333333",
  "lat": "-33.6666667"
}, {
  "name": "Mar\u00eda Pinto",
  "lng": "-71.1333333",
  "lat": "-33.5333333"
}, {
  "name": "Paine",
  "lng": "-70.7500000",
  "lat": "-33.8166667"
}, {
  "name": "Buin",
  "lng": "-70.7500000",
  "lat": "-33.7333333"
}, {
  "name": "Tiltil",
  "lng": "-70.9333333",
  "lat": "-33.0833333"
}, {
  "name": "Colina",
  "lng": "-70.6833333",
  "lat": "-33.2000000"
}, {
  "name": "Pirque",
  "lng": "-70.5500000",
  "lat": "-33.6333333"
}, {
  "name": "Vitacura",
  "lng": "-70.6000000",
  "lat": "-33.4000000"
}, {
  "name": "San Miguel",
  "lng": "-70.6666667",
  "lat": "-33.5000000"
}, {
  "name": "Renca",
  "lng": "-70.7333333",
  "lat": "-33.4000000"
}, {
  "name": "Quinta Normal",
  "lng": "-70.7000000",
  "lat": "-33.4500000"
}, {
  "name": "Pudahuel",
  "lng": "-70.7166667",
  "lat": "-33.4333333"
}, {
  "name": "Pe\u00f1alol\u00e9n",
  "lng": "-70.5333333",
  "lat": "-33.4833333"
}, {
  "name": "\u00d1u\u00f1oa",
  "lng": "-70.6000000",
  "lat": "-33.4666667"
}, {
  "name": "Macul",
  "lng": "-70.5666667",
  "lat": "-33.5000000"
}, {
  "name": "Lo Espejo",
  "lng": "-70.7166667",
  "lat": "-33.5333333"
}, {
  "name": "Las Condes",
  "lng": "-70.5833333",
  "lat": "-33.4166667"
}, {
  "name": "La Pintana",
  "lng": "-70.6166667",
  "lat": "-33.5833333"
}, {
  "name": "La Florida",
  "lng": "-70.5666667",
  "lat": "-33.5500000"
}, {
  "name": "Independencia",
  "lng": "-70.6549320",
  "lat": "-33.4219880"
}, {
  "name": "Estaci\u00f3n Central",
  "lng": "-70.7029760",
  "lat": "-33.4633150"
}, {
  "name": "Conchal\u00ed",
  "lng": "-70.6166667",
  "lat": "-33.3500000"
}, {
  "name": "Cerrillos",
  "lng": "-70.7000000",
  "lat": "-33.4833333"
}, {
  "name": "Arica",
  "lng": "-70.3144444",
  "lat": "-18.4750000"
}, {
  "name": "Camarones",
  "lng": "-69.8666667",
  "lat": "-19.0166667"
}, {
  "name": "Putre",
  "lng": "-69.5977778",
  "lat": "-18.1916667"
}, {
  "name": "General Lagos",
  "lng": "-69.5000000",
  "lat": "-17.5666667"
}, {
  "name": "Iquique",
  "lng": "-70.1666667",
  "lat": "-20.2166667"
}, {
  "name": "Alto Hospicio",
  "lng": "-70.1166667",
  "lat": "-20.2500000"
}, {
  "name": "Pozo Almonte",
  "lng": "-69.7833333",
  "lat": "-20.2666667"
}, {
  "name": "Cami\u00f1a",
  "lng": "-69.4166667",
  "lat": "-19.3000000"
}, {
  "name": "Colchane",
  "lng": "-68.6166667",
  "lat": "-19.2666667"
}, {
  "name": "Huara",
  "lng": "-69.7666667",
  "lat": "-19.9666667"
}, {
  "name": "Pica",
  "lng": "-69.3333333",
  "lat": "-20.5000000"
}, {
  "name": "Antofagasta",
  "lng": "-70.4000000",
  "lat": "-23.6333333"
}, {
  "name": "Mejillones",
  "lng": "-70.4500000",
  "lat": "-23.1000000"
}, {
  "name": "Sierra Gorda",
  "lng": "-69.3166667",
  "lat": "-22.8833333"
}, {
  "name": "Taltal",
  "lng": "-69.7666667",
  "lat": "-25.2833333"
}, {
  "name": "Calama",
  "lng": "-68.9166667",
  "lat": "-22.4666667"
}, {
  "name": "Ollague",
  "lng": "-68.2666667",
  "lat": "-21.2166667"
}, {
  "name": "San Pedro de Atacama",
  "lng": "-68.2166667",
  "lat": "-22.9166667"
}, {
  "name": "Mar\u00eda Elena",
  "lng": "-69.6666667",
  "lat": "-22.3500000"
}, {
  "name": "Tocopilla",
  "lng": "-70.2000000",
  "lat": "-22.0666667"
}, {
  "name": "Copiap\u00f3",
  "lng": "-70.3166667",
  "lat": "-27.3666667"
}, {
  "name": "Caldera",
  "lng": "-70.8166667",
  "lat": "-27.0666667"
}, {
  "name": "Tierra Amarilla",
  "lng": "-70.2666667",
  "lat": "-27.4666667"
}, {
  "name": "Cha\u00f1aral",
  "lng": "-70.6000000",
  "lat": "-26.3333333"
}, {
  "name": "Diego de Almagro",
  "lng": "-70.0500000",
  "lat": "-26.3666667"
}, {
  "name": "Vallenar",
  "lng": "-70.7500000",
  "lat": "-28.5666667"
}, {
  "name": "Alto del Carmen",
  "lng": "-70.4622222",
  "lat": "-28.9336111"
}, {
  "name": "Freirina",
  "lng": "-71.0666667",
  "lat": "-28.5000000"
}, {
  "name": "Huasco",
  "lng": "-71.2166667",
  "lat": "-28.4500000"
}, {
  "name": "R\u00edo Hurtado",
  "lng": "-70.7000000",
  "lat": "-30.2666667"
}, {
  "name": "Monte Patria",
  "lng": "-70.9333333",
  "lat": "-30.6833333"
}, {
  "name": "Ovalle",
  "lng": "-71.2000000",
  "lat": "-30.5833333"
}, {
  "name": "Los Vilos",
  "lng": "-71.5166667",
  "lat": "-31.9000000"
}, {
  "name": "Illapel",
  "lng": "-71.1500000",
  "lat": "-31.6166667"
}, {
  "name": "Paiguano",
  "lng": "-70.5166667",
  "lat": "-30.0166667"
}, {
  "name": "Andacollo",
  "lng": "-71.0833333",
  "lat": "-30.2166667"
}, {
  "name": "La Serena",
  "lng": "-71.2500000",
  "lat": "-29.9000000"
}, {
  "name": "Punitaqui",
  "lng": "-71.2666667",
  "lat": "-30.9000000"
}, {
  "name": "Combarbal\u00e1",
  "lng": "-71.0500000",
  "lat": "-31.1666667"
}, {
  "name": "Salamanca",
  "lng": "-70.9666667",
  "lat": "-31.7666667"
}, {
  "name": "Canela",
  "lng": "-71.4500000",
  "lat": "-31.4000000"
}, {
  "name": "Vicu\u00f1a",
  "lng": "-70.7000000",
  "lat": "-30.0166667"
}, {
  "name": "La Higuera",
  "lng": "-71.2666667",
  "lat": "-29.5000000"
}, {
  "name": "Coquimbo",
  "lng": "-71.3333333",
  "lat": "-29.9500000"
}, {
  "name": "Valpara\u00edso",
  "lng": "-71.6163889",
  "lat": "-33.0458333"
}, {
  "name": "Conc\u00f3n",
  "lng": "-71.5166667",
  "lat": "-32.9166667"
}, {
  "name": "Puchuncav\u00ed",
  "lng": "-71.4166667",
  "lat": "-32.7333333"
}, {
  "name": "Los Andes",
  "lng": "-70.6166667",
  "lat": "-32.8166667"
}, {
  "name": "Vi\u00f1a del Mar",
  "lng": "-71.5333333",
  "lat": "-33.0333333"
}, {
  "name": "Rinconada",
  "lng": "-70.7000000",
  "lat": "-32.8333333"
}, {
  "name": "La Ligua",
  "lng": "-71.2166667",
  "lat": "-32.4500000"
}, {
  "name": "Papudo",
  "lng": "-71.4500000",
  "lat": "-32.5166667"
}, {
  "name": "Zapallar",
  "lng": "-71.4666667",
  "lat": "-32.5333333"
}, {
  "name": "Calera",
  "lng": "-71.2166667",
  "lat": "-32.7833333"
}, {
  "name": "San Antonio",
  "lng": "-71.6166667",
  "lat": "-33.6000000"
}, {
  "name": "Cartagena",
  "lng": "-71.6000000",
  "lat": "-33.5500000"
}, {
  "name": "El Tabo",
  "lng": "-71.6666667",
  "lat": "-33.4500000"
}, {
  "name": "San Felipe",
  "lng": "-70.7333333",
  "lat": "-32.7500000"
}, {
  "name": "Llaillay",
  "lng": "-70.9666667",
  "lat": "-32.8500000"
}, {
  "name": "La Cruz",
  "lng": "-71.2333333",
  "lat": "-32.8166667"
}, {
  "name": "Villa Alemana",
  "lng": "-71.3666667",
  "lat": "-33.0500000"
}, {
  "name": "Limache",
  "lng": "-71.2833333",
  "lat": "-32.9833333"
}, {
  "name": "Putaendo",
  "lng": "-70.7333333",
  "lat": "-32.6333333"
}, {
  "name": "Olmu\u00e9",
  "lng": "-71.2000000",
  "lat": "-33.0000000"
}, {
  "name": "Quilpu\u00e9",
  "lng": "-71.4500000",
  "lat": "-33.0500000"
}, {
  "name": "Santa Mar\u00eda",
  "lng": "-70.6666667",
  "lat": "-32.7500000"
}, {
  "name": "Panquehue",
  "lng": "-70.8333333",
  "lat": "-32.8000000"
}, {
  "name": "Catemu",
  "lng": "-71.0333333",
  "lat": "-32.6333333"
}, {
  "name": "Santo Domingo",
  "lng": "-71.6500000",
  "lat": "-33.6333333"
}, {
  "name": "El Quisco",
  "lng": "-71.7000000",
  "lat": "-33.4000000"
}, {
  "name": "Algarrobo",
  "lng": "-71.6927778",
  "lat": "-33.3911111"
}, {
  "name": "Nogales",
  "lng": "-71.2333333",
  "lat": "-32.7166667"
}, {
  "name": "Hijuelas",
  "lng": "-71.1666667",
  "lat": "-32.8000000"
}, {
  "name": "Quillota",
  "lng": "-71.2666667",
  "lat": "-32.8833333"
}, {
  "name": "Petorca",
  "lng": "-70.9333333",
  "lat": "-32.2500000"
}, {
  "name": "Cabildo",
  "lng": "-71.1333333",
  "lat": "-32.4166667"
}, {
  "name": "San Esteban",
  "lng": "-70.5833333",
  "lat": "-32.8000000"
}, {
  "name": "Calle Larga",
  "lng": "-70.6333333",
  "lat": "-32.8500000"
}, {
  "name": "Isla de Pascua",
  "lng": "-109.3750000",
  "lat": "-27.0833333"
}, {
  "name": "Quintero",
  "lng": "-71.5333333",
  "lat": "-32.7833333"
}, {
  "name": "Juan Fern\u00e1ndez",
  "lng": "-78.8666667",
  "lat": "-33.6166667"
}, {
  "name": "Casablanca",
  "lng": "-71.4166667",
  "lat": "-33.3166667"
}, {
  "name": "Rancagua",
  "lng": "-70.7397222",
  "lat": "-34.1652778"
}, {
  "name": "Coinco",
  "lng": "-70.9666667",
  "lat": "-34.2666667"
}, {
  "name": "Do\u00f1ihue",
  "lng": "-70.9666667",
  "lat": "-34.2333333"
}, {
  "name": "Las Cabras",
  "lng": "-71.3166667",
  "lat": "-34.3000000"
}, {
  "name": "Malloa",
  "lng": "-70.9500000",
  "lat": "-34.4500000"
}, {
  "name": "Olivar",
  "lng": "-70.8175000",
  "lat": "-34.2100000"
}, {
  "name": "San Vicente",
  "lng": "-71.1333333",
  "lat": "-34.5000000"
}, {
  "name": "Marchihue",
  "lng": "-71.6333333",
  "lat": "-34.4000000"
}, {
  "name": "Paredones",
  "lng": "-71.1666667",
  "lat": "-34.7833333"
}, {
  "name": "Ch\u00e9pica",
  "lng": "-71.2833333",
  "lat": "-34.7333333"
}, {
  "name": "Lolol",
  "lng": "-71.6447222",
  "lat": "-34.7286111"
}, {
  "name": "Palmilla",
  "lng": "-71.3666667",
  "lat": "-34.6000000"
}, {
  "name": "Santa Cruz",
  "lng": "-71.3666667",
  "lat": "-34.6333333"
}, {
  "name": "Placilla",
  "lng": "-71.1166667",
  "lat": "-34.6333333"
}, {
  "name": "La Estrella",
  "lng": "-71.6666667",
  "lat": "-34.2000000"
}, {
  "name": "Rengo",
  "lng": "-70.8666667",
  "lat": "-34.4166667"
}, {
  "name": "Pichidegua",
  "lng": "-71.3000000",
  "lat": "-34.3500000"
}, {
  "name": "Pumanque",
  "lng": "-71.6666667",
  "lat": "-34.6000000"
}, {
  "name": "Peralillo",
  "lng": "-71.4833333",
  "lat": "-34.4833333"
}, {
  "name": "Nancagua",
  "lng": "-71.2166667",
  "lat": "-34.6666667"
}, {
  "name": "Chimbarongo",
  "lng": "-71.0500000",
  "lat": "-34.7000000"
}, {
  "name": "San Fernando",
  "lng": "-70.9666667",
  "lat": "-34.5833333"
}, {
  "name": "Navidad",
  "lng": "-71.8333333",
  "lat": "-33.9333333"
}, {
  "name": "Litueche",
  "lng": "-71.7333333",
  "lat": "-34.1166667"
}, {
  "name": "Pichilemu",
  "lng": "-72.0000000",
  "lat": "-34.3833333"
}, {
  "name": "Requ\u00ednoa",
  "lng": "-70.8333333",
  "lat": "-34.2833333"
}, {
  "name": "Quinta de Tilcoco",
  "lng": "-70.9833333",
  "lat": "-34.3500000"
}, {
  "name": "Peumo",
  "lng": "-71.1666667",
  "lat": "-34.4000000"
}, {
  "name": "Mostazal",
  "lng": "-70.7000000",
  "lat": "-33.9833333"
}, {
  "name": "Machal\u00ed",
  "lng": "-70.6511111",
  "lat": "-34.1825000"
}, {
  "name": "Graneros",
  "lng": "-70.7266667",
  "lat": "-34.0647222"
}, {
  "name": "Coltauco",
  "lng": "-71.0857230",
  "lat": "34.2872290"
}, {
  "name": "Codegua",
  "lng": "-70.6666667",
  "lat": "-34.0333333"
}, {
  "name": "Talca",
  "lng": "-71.6666667",
  "lat": "-35.4333333"
}, {
  "name": "Curepto",
  "lng": "-72.0166667",
  "lat": "-35.0833333"
}, {
  "name": "Maule",
  "lng": "-71.7000000",
  "lat": "-35.5333333"
}, {
  "name": "Pencahue",
  "lng": "-71.8166667",
  "lat": "-35.4000000"
}, {
  "name": "San Clemente",
  "lng": "-71.4833333",
  "lat": "-35.5500000"
}, {
  "name": "Cauquenes",
  "lng": "-72.3500000",
  "lat": "-35.9666667"
}, {
  "name": "Pelluhue",
  "lng": "-72.6333333",
  "lat": "-35.8333333"
}, {
  "name": "Huala\u00f1\u00e9",
  "lng": "-71.8047222",
  "lat": "-34.9766667"
}, {
  "name": "Molina",
  "lng": "-71.2833333",
  "lat": "-34.1166667"
}, {
  "name": "Romeral",
  "lng": "-71.1333333",
  "lat": "-34.9666667"
}, {
  "name": "Teno",
  "lng": "-71.1833333",
  "lat": "-34.8666667"
}, {
  "name": "Linares",
  "lng": "-71.6000000",
  "lat": "-35.8500000"
}, {
  "name": "Longav\u00ed",
  "lng": "-71.6833333",
  "lat": "-35.9666667"
}, {
  "name": "Retiro",
  "lng": "-71.7666667",
  "lat": "-36.0500000"
}, {
  "name": "Villa Alegre",
  "lng": "-71.7500000",
  "lat": "-35.6666667"
}, {
  "name": "Constituci\u00f3n",
  "lng": "-72.4166667",
  "lat": "-35.3333333"
}, {
  "name": "Empedrado",
  "lng": "-72.2833333",
  "lat": "-35.6000000"
}, {
  "name": "Pelarco",
  "lng": "-71.4500000",
  "lat": "-35.3833333"
}, {
  "name": "R\u00edo Claro",
  "lng": "-71.2666667",
  "lat": "-35.2833333"
}, {
  "name": "San Rafael",
  "lng": "-71.5333333",
  "lat": "-35.3166667"
}, {
  "name": "Curic\u00f3",
  "lng": "-71.2333333",
  "lat": "-34.9833333"
}, {
  "name": "Chanco",
  "lng": "-72.5333333",
  "lat": "-35.7333333"
}, {
  "name": "Licant\u00e9n",
  "lng": "-72.0000000",
  "lat": "-34.9833333"
}, {
  "name": "Rauco",
  "lng": "-71.3166667",
  "lat": "-34.9333333"
}, {
  "name": "Sagrada Familia",
  "lng": "-71.3833333",
  "lat": "-35.0000000"
}, {
  "name": "Vichuqu\u00e9n",
  "lng": "-72.0000000",
  "lat": "-34.8833333"
}, {
  "name": "Colb\u00fan",
  "lng": "-71.4166667",
  "lat": "-35.7000000"
}, {
  "name": "Parral",
  "lng": "-71.8333333",
  "lat": "-36.1500000"
}, {
  "name": "San Javier",
  "lng": "-71.7500000",
  "lat": "-35.6000000"
}, {
  "name": "Yerbas Buenas",
  "lng": "-71.5833333",
  "lat": "-35.7500000"
}, {
  "name": "Concepci\u00f3n",
  "lng": "-73.0500000",
  "lat": "-36.8333333"
}, {
  "name": "Chiguayante",
  "lng": "-73.0166667",
  "lat": "-36.9166667"
}, {
  "name": "Hualqui",
  "lng": "-72.9333333",
  "lat": "-36.9666667"
}, {
  "name": "Penco",
  "lng": "-72.9833333",
  "lat": "-36.7333333"
}, {
  "name": "Santa Juana",
  "lng": "-72.9333333",
  "lat": "-37.1666667"
}, {
  "name": "Tom\u00e9",
  "lng": "-72.9500000",
  "lat": "-36.6166667"
}, {
  "name": "Lebu",
  "lng": "-73.6500000",
  "lat": "-37.6166667"
}, {
  "name": "Ca\u00f1ete",
  "lng": "-73.3833333",
  "lat": "-37.8000000"
}, {
  "name": "Curanilahue",
  "lng": "-73.3500000",
  "lat": "-37.4666667"
}, {
  "name": "Tir\u00faa",
  "lng": "-73.5000000",
  "lat": "-38.3333333"
}, {
  "name": "Antuco",
  "lng": "-71.6833333",
  "lat": "-37.3333333"
}, {
  "name": "Laja",
  "lng": "-72.7000000",
  "lat": "-37.2666667"
}, {
  "name": "Nacimiento",
  "lng": "-72.6666667",
  "lat": "-37.5000000"
}, {
  "name": "Quilaco",
  "lng": "-71.9833333",
  "lat": "-37.6666667"
}, {
  "name": "San Rosendo",
  "lng": "-72.7166667",
  "lat": "-37.2666667"
}, {
  "name": "Tucapel",
  "lng": "-71.9500000",
  "lat": "-37.2833333"
}, {
  "name": "Alto Biob\u00edo",
  "lng": "-71.3166667",
  "lat": "-38.0500000"
}, {
  "name": "Bulnes",
  "lng": "-72.3014290",
  "lat": "-36.7419870"
}, {
  "name": "Coelemu",
  "lng": "-72.7000000",
  "lat": "-36.4833333"
}, {
  "name": "Chill\u00e1n Viejo",
  "lng": "-72.1333333",
  "lat": "-36.6166667"
}, {
  "name": "Ninhue",
  "lng": "-72.4000000",
  "lat": "-36.4000000"
}, {
  "name": "Pemuco",
  "lng": "-72.1000000",
  "lat": "-36.9666667"
}, {
  "name": "Portezuelo",
  "lng": "-72.4333333",
  "lat": "-36.5333333"
}, {
  "name": "Quirihue",
  "lng": "-72.5333333",
  "lat": "-36.2833333"
}, {
  "name": "Treguaco",
  "lng": "-72.6666667",
  "lat": "-36.4333333"
}, {
  "name": "San Ignacio",
  "lng": "-72.0333333",
  "lat": "-36.8000000"
}, {
  "name": "San Carlos",
  "lng": "-71.9580556",
  "lat": "-36.4247222"
}, {
  "name": "Yungay",
  "lng": "-72.0166667",
  "lat": "-37.1166667"
}, {
  "name": "San Nicol\u00e1s",
  "lng": "-72.2166667",
  "lat": "-36.5000000"
}, {
  "name": "San Fabi\u00e1n",
  "lng": "-71.5500000",
  "lat": "-36.5500000"
}, {
  "name": "R\u00e1nquil",
  "lng": "-72.5500000",
  "lat": "-36.6500000"
}, {
  "name": "Quill\u00f3n",
  "lng": "-72.4666667",
  "lat": "-36.7333333"
}, {
  "name": "Pinto",
  "lng": "-71.9000000",
  "lat": "-36.7000000"
}, {
  "name": "\u00d1iqu\u00e9n",
  "lng": "-71.9000000",
  "lat": "-36.3000000"
}, {
  "name": "El Carmen",
  "lng": "-72.0323130",
  "lat": "-36.8994440"
}, {
  "name": "Coihueco",
  "lng": "-71.8333333",
  "lat": "-36.6166667"
}, {
  "name": "Cobquecura",
  "lng": "-72.7833333",
  "lat": "-36.1333333"
}, {
  "name": "Chill\u00e1n",
  "lng": "-72.1166667",
  "lat": "-36.6000000"
}, {
  "name": "Yumbel",
  "lng": "-72.5333333",
  "lat": "-37.1333333"
}, {
  "name": "Santa B\u00e1rbara",
  "lng": "-72.0166667",
  "lat": "-37.6666667"
}, {
  "name": "Quilleco",
  "lng": "-71.9666667",
  "lat": "-37.4666667"
}, {
  "name": "Negrete",
  "lng": "-72.5166667",
  "lat": "-37.5833333"
}, {
  "name": "Mulch\u00e9n",
  "lng": "-72.2333333",
  "lat": "-37.7166667"
}, {
  "name": "Cabrero",
  "lng": "-72.4000000",
  "lat": "-37.0333333"
}, {
  "name": "Los Angeles",
  "lng": "-72.3500000",
  "lat": "-37.4666667"
}, {
  "name": "Los Alamos",
  "lng": "-73.4666667",
  "lat": "-37.6166667"
}, {
  "name": "Contulmo",
  "lng": "-73.2333333",
  "lat": "-38.0000000"
}, {
  "name": "Arauco",
  "lng": "-73.3166667",
  "lat": "-37.2500000"
}, {
  "name": "Hualp\u00e9n",
  "lng": "-73.0833333",
  "lat": "-36.7833333"
}, {
  "name": "Talcahuano",
  "lng": "-73.1166667",
  "lat": "-36.7166667"
}, {
  "name": "San Pedro de la Paz",
  "lng": "-73.1166667",
  "lat": "-36.8333333"
}, {
  "name": "Lota",
  "lng": "-73.1560560",
  "lat": "-37.0870730"
}, {
  "name": "Florida",
  "lng": "-72.6666667",
  "lat": "-36.8166667"
}, {
  "name": "Coronel",
  "lng": "-73.1333333",
  "lat": "-37.0166667"
}, {
  "name": "Temuco",
  "lng": "-72.6666667",
  "lat": "-38.7500000"
}, {
  "name": "Cunco",
  "lng": "-72.0333333",
  "lat": "-38.9166667"
}, {
  "name": "Freire",
  "lng": "-72.6333333",
  "lat": "-38.9500000"
}, {
  "name": "Gorbea",
  "lng": "-72.6833333",
  "lat": "-39.1000000"
}, {
  "name": "Loncoche",
  "lng": "-72.6333333",
  "lat": "-39.3666667"
}, {
  "name": "Nueva Imperial",
  "lng": "-72.9500000",
  "lat": "-38.7333333"
}, {
  "name": "Perquenco",
  "lng": "-72.3833333",
  "lat": "-38.4166667"
}, {
  "name": "Puc\u00f3n",
  "lng": "-71.9666667",
  "lat": "-39.2666667"
}, {
  "name": "Teodoro Schmidt",
  "lng": "-73.0500000",
  "lat": "-38.9666667"
}, {
  "name": "Vilc\u00fan",
  "lng": "-72.3794444",
  "lat": "-39.1183333"
}, {
  "name": "Cholchol",
  "lng": "-72.8500000",
  "lat": "-38.6000000"
}, {
  "name": "Collipulli",
  "lng": "-72.4333333",
  "lat": "-37.9500000"
}, {
  "name": "Ercilla",
  "lng": "-72.3833333",
  "lat": "-38.0500000"
}, {
  "name": "Los Sauces",
  "lng": "-72.8333333",
  "lat": "-37.9666667"
}, {
  "name": "Pur\u00e9n",
  "lng": "-73.0833333",
  "lat": "-38.0166667"
}, {
  "name": "Traigu\u00e9n",
  "lng": "-72.6833333",
  "lat": "-38.2500000"
}, {
  "name": "Carahue",
  "lng": "-73.1666667",
  "lat": "-38.7000000"
}, {
  "name": "Curarrehue",
  "lng": "-71.5833333",
  "lat": "-39.3500000"
}, {
  "name": "Galvarino",
  "lng": "-72.7833333",
  "lat": "-38.4000000"
}, {
  "name": "Lautaro",
  "lng": "-72.4350000",
  "lat": "-38.5291667"
}, {
  "name": "Padre Las Casas",
  "lng": "-72.6000000",
  "lat": "-38.7666667"
}, {
  "name": "Pitrufqu\u00e9n",
  "lng": "-72.6500000",
  "lat": "-38.9833333"
}, {
  "name": "Tolt\u00e9n",
  "lng": "-73.2333333",
  "lat": "-39.2166667"
}, {
  "name": "Villarrica",
  "lng": "-72.2166667",
  "lat": "-39.2666667"
}, {
  "name": "Angol",
  "lng": "-72.7166667",
  "lat": "-37.8000000"
}, {
  "name": "Curacaut\u00edn",
  "lng": "-71.8833333",
  "lat": "-38.4333333"
}, {
  "name": "Lonquimay",
  "lng": "-71.2333333",
  "lat": "-38.4333333"
}, {
  "name": "Lumaco",
  "lng": "-72.9166667",
  "lat": "-38.1500000"
}, {
  "name": "Renaico",
  "lng": "-72.5833333",
  "lat": "-37.6666667"
}, {
  "name": "Victoria",
  "lng": "-72.3333333",
  "lat": "-38.2166667"
}, {
  "name": "Saavedra",
  "lng": "-73.4000000",
  "lat": "-38.7833333"
}, {
  "name": "Melipeuco",
  "lng": "-71.7000000",
  "lat": "-38.8500000"
}, {
  "name": "Valdivia",
  "lng": "-73.2333333",
  "lat": "-39.8000000"
}, {
  "name": "Corral",
  "lng": "-73.4333333",
  "lat": "-39.8666667"
}, {
  "name": "Lanco",
  "lng": "-72.7666667",
  "lat": "-39.4333333"
}, {
  "name": "Los Lagos",
  "lng": "-72.8333333",
  "lat": "-39.8500000"
}, {
  "name": "M\u00e1fil",
  "lng": "-72.9500000",
  "lat": "-39.6500000"
}, {
  "name": "Mariquina",
  "lng": "-72.9666667",
  "lat": "-39.5166667"
}, {
  "name": "Paillaco",
  "lng": "-72.8833333",
  "lat": "-40.0666667"
}, {
  "name": "Panguipulli",
  "lng": "-72.3333333",
  "lat": "-39.6333333"
}, {
  "name": "La Uni\u00f3n",
  "lng": "-73.0833333",
  "lat": "-40.2833333"
}, {
  "name": "Futrono",
  "lng": "-72.4000000",
  "lat": "-40.1333333"
}, {
  "name": "Lago Ranco",
  "lng": "-72.5000000",
  "lat": "-40.3166667"
}, {
  "name": "R\u00edo Bueno",
  "lng": "-72.9666667",
  "lat": "-40.3166667"
}, {
  "name": "Puerto Montt",
  "lng": "-72.9333333",
  "lat": "-41.4666667"
}, {
  "name": "Cocham\u00f3",
  "lng": "-72.3166667",
  "lat": "-41.5000000"
}, {
  "name": "Frutillar",
  "lng": "-73.1000000",
  "lat": "-41.1166667"
}, {
  "name": "Puerto Varas",
  "lng": "-72.9833333",
  "lat": "-41.3166667"
}, {
  "name": "Ancud",
  "lng": "-73.8333333",
  "lat": "-41.8666667"
}, {
  "name": "Curaco de V\u00e9lez",
  "lng": "-73.5833333",
  "lat": "-42.4333333"
}, {
  "name": "Puqueld\u00f3n",
  "lng": "-73.6333333",
  "lat": "-42.5833333"
}, {
  "name": "Quell\u00f3n",
  "lng": "-73.6000000",
  "lat": "-43.1000000"
}, {
  "name": "Quinchao",
  "lng": "-73.4166667",
  "lat": "-42.5333333"
}, {
  "name": "Puerto Octay",
  "lng": "-72.9000000",
  "lat": "-40.9666667"
}, {
  "name": "Puyehue",
  "lng": "-72.6166667",
  "lat": "-40.6666667"
}, {
  "name": "Hualaihu\u00e9",
  "lng": "-72.6833333",
  "lat": "-42.0166667"
}, {
  "name": "Chait\u00e9n",
  "lng": "-72.7088889",
  "lat": "-42.9194444"
}, {
  "name": "San Juan de la Costa",
  "lng": "-73.4000000",
  "lat": "-40.5166667"
}, {
  "name": "Llanquihue",
  "lng": "-73.0166667",
  "lat": "-41.2500000"
}, {
  "name": "Calbuco",
  "lng": "-73.1333333",
  "lat": "-41.7666667"
}, {
  "name": "Fresia",
  "lng": "-73.4500000",
  "lat": "-41.1500000"
}, {
  "name": "Los Muermos",
  "lng": "-73.4833333",
  "lat": "-41.4000000"
}, {
  "name": "Maull\u00edn",
  "lng": "-73.6000000",
  "lat": "-41.6166667"
}, {
  "name": "Castro",
  "lng": "-73.8000000",
  "lat": "-42.4666667"
}, {
  "name": "Queil\u00e9n",
  "lng": "-73.4666667",
  "lat": "-42.8666667"
}, {
  "name": "Quemchi",
  "lng": "-73.5166667",
  "lat": "-42.1333333"
}, {
  "name": "Osorno",
  "lng": "-73.1500000",
  "lat": "-40.5666667"
}, {
  "name": "Purranque",
  "lng": "-73.1666667",
  "lat": "-40.9166667"
}, {
  "name": "R\u00edo Negro",
  "lng": "-73.2333333",
  "lat": "-40.7833333"
}, {
  "name": "San Pablo",
  "lng": "-73.0166667",
  "lat": "-40.4000000"
}, {
  "name": "Futaleuf\u00fa",
  "lng": "-71.8500000",
  "lat": "-43.1666667"
}, {
  "name": "Palena",
  "lng": "-71.8000000",
  "lat": "-43.6166667"
}, {
  "name": "Dalcahue",
  "lng": "-73.7000000",
  "lat": "-42.3666667"
}, {
  "name": "Chonchi",
  "lng": "-73.8166667",
  "lat": "-42.6166667"
}, {
  "name": "Coyhaique",
  "lng": "-72.0666667",
  "lat": "-45.5666667"
}, {
  "name": "Ais\u00e9n",
  "lng": "-72.7000000",
  "lat": "-45.4000000"
}, {
  "name": "Guaitecas",
  "lng": "-73.7333333",
  "lat": "-43.8833333"
}, {
  "name": "O'Higgins",
  "lng": "-72.5666667",
  "lat": "-48.4666667"
}, {
  "name": "Chile Chico",
  "lng": "-71.7333333",
  "lat": "-46.5500000"
}, {
  "name": "Verde",
  "lng": "-71.8333333",
  "lat": "-44.2333333"
}, {
  "name": "Cisnes",
  "lng": "-72.7000000",
  "lat": "-44.7500000"
}, {
  "name": "Cochrane",
  "lng": "-72.5500000",
  "lat": "-47.2666667"
}, {
  "name": "Tortel",
  "lng": "-73.5666667",
  "lat": "-47.8333333"
}, {
  "name": "R\u00edo Ib\u00e1\u00f1ez",
  "lng": "-71.9333333",
  "lat": "-46.3000000"
}, {
  "name": "Punta Arenas",
  "lng": "-70.9336111",
  "lat": "-53.1669444"
}, {
  "name": "R\u00edo Verde",
  "lng": "-71.4833333",
  "lat": "-52.6500000"
}, {
  "name": "Cabo de Hornos (Ex-Navarino)",
  "lng": "-67.6166667",
  "lat": "-54.9333333"
}, {
  "name": "Porvenir",
  "lng": "-70.3666667",
  "lat": "-53.3000000"
}, {
  "name": "Timaukel",
  "lng": "-69.9000000",
  "lat": "-53.6666667"
}, {
  "name": "Torres del Paine",
  "lng": "-72.3500000",
  "lat": "-51.2666667"
}, {
  "name": "Natales",
  "lng": "-72.5166667",
  "lat": "-51.7333333"
}, {
  "name": "Primavera",
  "lng": "-69.2500000",
  "lat": "-52.7166667"
}, {
  "name": "Ant\u00e1rtica",
  "lng": "-71.5000000",
  "lat": "-75.0000000"
}, {
  "name": "San Gregorio",
  "lng": "-69.6833333",
  "lat": "-52.3166667"
}, {
  "name": "Laguna Blanca",
  "lng": "-71.9166667",
  "lat": "-52.2500000"
}]
Error.destroy_all
Comuna.all.each do |comuna|
  found = coords.select{|c| c[:name] == comuna.nombre}
  p found
  if found.count > 0
    comuna.latitud = found.first[:lat]
    comuna.longitud = found.first[:lng]
    comuna.save
  else
    Error.create(modelo: "Coordenadas", registro: comuna.nombre, errores: "No se ha encontrado comuna en array de coordenadas")
  end
end
