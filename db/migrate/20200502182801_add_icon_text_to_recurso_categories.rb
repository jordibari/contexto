class AddIconTextToRecursoCategories < ActiveRecord::Migration[6.0]
  def change
    add_column :recurso_categories, :icon_text, :string
  end
end
