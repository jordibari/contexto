class CreateDistritos < ActiveRecord::Migration[6.0]
  def change
    create_table :distritos do |t|
      t.string :nombre
      t.references :circunscripcion, null: false, foreign_key: true

      t.timestamps
    end
  end
end
