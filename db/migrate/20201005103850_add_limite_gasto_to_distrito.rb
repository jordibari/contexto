class AddLimiteGastoToDistrito < ActiveRecord::Migration[6.0]
  def change
    add_column :distritos, :limite_gasto, :integer
  end
end
