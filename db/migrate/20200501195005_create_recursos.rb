class CreateRecursos < ActiveRecord::Migration[6.0]
  def change
    create_table :recursos do |t|
      t.string :url
      t.string :description_short      
      t.string :description_long
      t.string :reading_time
      t.string :dificulty      
      t.boolean :recommended
      t.text :resume
      t.timestamps
    end
  end
end
