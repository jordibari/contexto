class AddOrganizationRefToMembers < ActiveRecord::Migration[6.0]
  def change
    add_reference :members, :organization, null: true, foreign_key: true
  end
end
