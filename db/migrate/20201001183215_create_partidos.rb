class CreatePartidos < ActiveRecord::Migration[6.0]
  def change
    create_table :partidos do |t|
      t.string :nombre
      # t.integer :ano_eleccion
      # t.references :pacto, null: false, foreign_key: true

      t.timestamps
    end
  end
end
