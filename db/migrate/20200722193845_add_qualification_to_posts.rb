class AddQualificationToPosts < ActiveRecord::Migration[6.0]
  def change
    add_reference :posts, :qualification, null: true, foreign_key: true
  end
end
