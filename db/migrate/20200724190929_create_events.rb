class CreateEvents < ActiveRecord::Migration[6.0]
  def change
    create_table :events do |t|
      t.string :name
      t.date :fecha
      t.string :url

      t.timestamps
    end
  end
end
