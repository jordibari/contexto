class AddColorToRecursoCategories < ActiveRecord::Migration[6.0]
  def change
    add_column :recurso_categories, :color, :string
  end
end
