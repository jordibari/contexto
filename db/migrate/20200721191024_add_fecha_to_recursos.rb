class AddFechaToRecursos < ActiveRecord::Migration[6.0]
  def change
    add_column :recursos, :fecha, :date
  end
end
