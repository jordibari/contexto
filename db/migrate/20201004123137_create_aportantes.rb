class CreateAportantes < ActiveRecord::Migration[6.0]
  def change
    create_table :aportantes do |t|
      t.string :nombre

      t.timestamps
    end
  end
end
