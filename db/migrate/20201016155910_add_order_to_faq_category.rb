class AddOrderToFaqCategory < ActiveRecord::Migration[6.0]
  def change
    add_column :faq_categories, :order, :integer
  end
end
