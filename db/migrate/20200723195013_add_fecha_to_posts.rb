class AddFechaToPosts < ActiveRecord::Migration[6.0]
  def change
    add_column :posts, :fecha, :date
  end
end
