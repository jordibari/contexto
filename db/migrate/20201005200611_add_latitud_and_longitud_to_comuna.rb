class AddLatitudAndLongitudToComuna < ActiveRecord::Migration[6.0]
  def change
    add_column :comunas, :latitud, :float
    add_column :comunas, :longitud, :float
  end
end
