class AddOrderToRecursoCategory < ActiveRecord::Migration[6.0]
  def change
    add_column :recurso_categories, :order, :integer
  end
end
