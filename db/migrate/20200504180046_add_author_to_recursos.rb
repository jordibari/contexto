class AddAuthorToRecursos < ActiveRecord::Migration[6.0]
  def change
    add_column :recursos, :author, :string
  end
end
