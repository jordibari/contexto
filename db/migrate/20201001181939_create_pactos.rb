class CreatePactos < ActiveRecord::Migration[6.0]
  def change
    create_table :pactos do |t|
      t.string :nombre
      t.integer :ano_eleccion

      t.timestamps
    end
  end
end
