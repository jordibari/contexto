class AddDestacadoToPost < ActiveRecord::Migration[6.0]
  def change
    add_column :posts, :destacado, :boolean
  end
end
