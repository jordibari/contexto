class CreateCandidatos < ActiveRecord::Migration[6.0]
  def change
    create_table :candidatos do |t|
      t.string :nombre
      t.references :partido, null: false, foreign_key: true
      # t.references :pacto, null: true, foreign_key: true
      t.references :distrito, null: false, foreign_key: true
      t.string :intereses
      t.string :url

      t.timestamps
    end
  end
end
