class AddColorToPactos < ActiveRecord::Migration[6.0]
  def change
    add_column :pactos, :color, :string
  end
end
