class CreateErrors < ActiveRecord::Migration[6.0]
  def change
    create_table :errors do |t|
      t.json :registro
      t.string :modelo
      t.string :nombe_fichero
      t.string :errores

      t.timestamps
    end
  end
end
