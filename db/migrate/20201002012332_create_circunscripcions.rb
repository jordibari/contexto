class CreateCircunscripcions < ActiveRecord::Migration[6.0]
  def change
    create_table :circunscripcions do |t|
      t.string :nombre

      t.timestamps
    end
  end
end
