class CreateCouncilMembers < ActiveRecord::Migration[6.0]
  def change
    create_table :council_members do |t|
      t.string :name
      t.string :especialidad
      t.string :descripcion

      t.timestamps
    end
  end
end
