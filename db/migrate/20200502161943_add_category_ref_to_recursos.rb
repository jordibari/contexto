class AddCategoryRefToRecursos < ActiveRecord::Migration[6.0]
  def change
    add_reference :recursos, :recurso_category, foreign_key: true
  end
end
