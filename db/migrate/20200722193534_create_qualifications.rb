class CreateQualifications < ActiveRecord::Migration[6.0]
  def change
    create_table :qualifications do |t|
      t.string :name
      t.string :label
      t.string :color

      t.timestamps
    end
  end
end
