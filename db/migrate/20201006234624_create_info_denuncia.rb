class CreateInfoDenuncia < ActiveRecord::Migration[6.0]
  def change
    create_table :info_denuncia do |t|
      t.string :descripcion
      t.integer :admisibles
      t.integer :inadmisibles
      t.integer :en_analisis

      t.timestamps
    end
  end
end
