class AddVideoToRecursos < ActiveRecord::Migration[6.0]
  def change
    add_column :recursos, :video, :string, null: true
  end
end
