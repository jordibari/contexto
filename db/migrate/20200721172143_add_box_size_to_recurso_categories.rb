class AddBoxSizeToRecursoCategories < ActiveRecord::Migration[6.0]
  def change
    add_column :recurso_categories, :box_size, :integer, :default => -1
  end
end
