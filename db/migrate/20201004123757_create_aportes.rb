class CreateAportes < ActiveRecord::Migration[6.0]
  def change
    create_table :aportes do |t|
      t.references :candidato, null: false, foreign_key: true
      t.references :aportante, null: false, foreign_key: true
      t.integer :tipo_de_aporte
      t.date :fecha
      t.integer :monto

      t.timestamps
    end
  end
end
