class CreateRecursoCategories < ActiveRecord::Migration[6.0]
  def change
    create_table :recurso_categories do |t|
      t.string :name
      t.string :description_short

      t.timestamps
    end
  end
end
