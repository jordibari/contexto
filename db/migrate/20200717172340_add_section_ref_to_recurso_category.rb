class AddSectionRefToRecursoCategory < ActiveRecord::Migration[6.0]
  def change
    add_reference :recurso_categories, :section, null: false, default: 1, foreign_key: true
  end
end
