class CreatePueblos < ActiveRecord::Migration[6.0]
  def change
    create_table :pueblos do |t|
      t.string :nombre

      t.timestamps
    end
  end
end
