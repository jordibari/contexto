class CreateLugars < ActiveRecord::Migration[6.0]
  def change
    create_table :lugars do |t|
      t.string :nombre
      t.references :comuna, null: false, foreign_key: true
      t.float :latitud
      t.float :longitud

      t.timestamps
    end
  end
end
