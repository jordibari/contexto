class AddCompromisoToCandidatos < ActiveRecord::Migration[6.0]
  def change
    add_column :candidatos, :compromiso, :boolean, :default => false
  end
end
