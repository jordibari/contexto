class AddTipoToOrganization < ActiveRecord::Migration[6.0]
  def change
    add_column :organizations, :tipo, :integer, :default => 1
  end
end
