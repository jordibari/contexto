class AddSocialNetworkDataToRecursos < ActiveRecord::Migration[6.0]
  def change
    add_column :recursos, :facebook, :string, null: true
    add_column :recursos, :twitter, :string, null: true
    add_column :recursos, :instagram, :string, null: true
  end
end
