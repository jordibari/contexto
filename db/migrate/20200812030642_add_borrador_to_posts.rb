class AddBorradorToPosts < ActiveRecord::Migration[6.0]
  def change
    add_column :posts, :borrador, :boolean, default: true
  end
end
