# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `rails
# db:schema:load`. When creating a new database, `rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2021_02_15_211815) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "pg_trgm"
  enable_extension "plpgsql"

  create_table "action_text_rich_texts", force: :cascade do |t|
    t.string "name", null: false
    t.text "body"
    t.string "record_type", null: false
    t.bigint "record_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["record_type", "record_id", "name"], name: "index_action_text_rich_texts_uniqueness", unique: true
  end

  create_table "active_storage_attachments", force: :cascade do |t|
    t.string "name", null: false
    t.string "record_type", null: false
    t.bigint "record_id", null: false
    t.bigint "blob_id", null: false
    t.datetime "created_at", null: false
    t.index ["blob_id"], name: "index_active_storage_attachments_on_blob_id"
    t.index ["record_type", "record_id", "name", "blob_id"], name: "index_active_storage_attachments_uniqueness", unique: true
  end

  create_table "active_storage_blobs", force: :cascade do |t|
    t.string "key", null: false
    t.string "filename", null: false
    t.string "content_type"
    t.text "metadata"
    t.bigint "byte_size", null: false
    t.string "checksum", null: false
    t.datetime "created_at", null: false
    t.index ["key"], name: "index_active_storage_blobs_on_key", unique: true
  end

  create_table "aportantes", force: :cascade do |t|
    t.string "nombre"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "aportes", force: :cascade do |t|
    t.bigint "candidato_id", null: false
    t.bigint "aportante_id", null: false
    t.integer "tipo_de_aporte"
    t.date "fecha"
    t.integer "monto"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["aportante_id"], name: "index_aportes_on_aportante_id"
    t.index ["candidato_id"], name: "index_aportes_on_candidato_id"
  end

  create_table "candidatos", force: :cascade do |t|
    t.string "nombre"
    t.bigint "partido_id", null: false
    t.bigint "distrito_id", null: false
    t.string "intereses"
    t.string "url"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.bigint "pacto_id"
    t.boolean "compromiso", default: false
    t.bigint "pueblo_id"
    t.index ["distrito_id"], name: "index_candidatos_on_distrito_id"
    t.index ["pacto_id"], name: "index_candidatos_on_pacto_id"
    t.index ["partido_id"], name: "index_candidatos_on_partido_id"
    t.index ["pueblo_id"], name: "index_candidatos_on_pueblo_id"
  end

  create_table "circunscripcions", force: :cascade do |t|
    t.string "nombre"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "comunas", force: :cascade do |t|
    t.string "nombre"
    t.bigint "provincia_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.bigint "distrito_id", null: false
    t.float "latitud"
    t.float "longitud"
    t.index ["distrito_id"], name: "index_comunas_on_distrito_id"
    t.index ["provincia_id"], name: "index_comunas_on_provincia_id"
  end

  create_table "council_members", force: :cascade do |t|
    t.string "name"
    t.string "especialidad"
    t.string "descripcion"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "distritos", force: :cascade do |t|
    t.string "nombre"
    t.bigint "circunscripcion_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.integer "limite_gasto"
    t.index ["circunscripcion_id"], name: "index_distritos_on_circunscripcion_id"
  end

  create_table "errors", force: :cascade do |t|
    t.json "registro"
    t.string "modelo"
    t.string "nombe_fichero"
    t.string "errores"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "events", force: :cascade do |t|
    t.string "name"
    t.date "fecha"
    t.string "url"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.string "lugar"
    t.integer "valor"
    t.time "hora"
  end

  create_table "faq_categories", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.integer "order"
  end

  create_table "faqs", force: :cascade do |t|
    t.text "question"
    t.text "answer"
    t.bigint "faq_category_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["faq_category_id"], name: "index_faqs_on_faq_category_id"
  end

  create_table "ficheros", force: :cascade do |t|
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "gutentag_taggings", id: :serial, force: :cascade do |t|
    t.integer "tag_id", null: false
    t.integer "taggable_id", null: false
    t.string "taggable_type", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["tag_id"], name: "index_gutentag_taggings_on_tag_id"
    t.index ["taggable_type", "taggable_id", "tag_id"], name: "unique_taggings", unique: true
    t.index ["taggable_type", "taggable_id"], name: "index_gutentag_taggings_on_taggable_type_and_taggable_id"
  end

  create_table "gutentag_tags", id: :serial, force: :cascade do |t|
    t.string "name", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "taggings_count", default: 0, null: false
    t.index ["name"], name: "index_gutentag_tags_on_name", unique: true
    t.index ["taggings_count"], name: "index_gutentag_tags_on_taggings_count"
  end

  create_table "info_denuncia", force: :cascade do |t|
    t.string "descripcion"
    t.integer "admisibles"
    t.integer "inadmisibles"
    t.integer "en_analisis"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "lugars", force: :cascade do |t|
    t.string "nombre"
    t.bigint "comuna_id", null: false
    t.float "latitud"
    t.float "longitud"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["comuna_id"], name: "index_lugars_on_comuna_id"
  end

  create_table "members", force: :cascade do |t|
    t.string "name"
    t.string "position"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.bigint "organization_id"
    t.index ["organization_id"], name: "index_members_on_organization_id"
  end

  create_table "organizations", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.string "description"
    t.string "url"
    t.integer "tipo", default: 1
  end

  create_table "pactos", force: :cascade do |t|
    t.string "nombre"
    t.integer "ano_eleccion"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.string "color"
  end

  create_table "partidos", force: :cascade do |t|
    t.string "nombre"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "pg_search_documents", force: :cascade do |t|
    t.text "content"
    t.string "searchable_type"
    t.bigint "searchable_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["searchable_type", "searchable_id"], name: "index_pg_search_documents_on_searchable_type_and_searchable_id"
  end

  create_table "post_categories", force: :cascade do |t|
    t.string "name"
    t.string "description"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "posts", force: :cascade do |t|
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.bigint "qualification_id"
    t.string "title"
    t.bigint "post_category_id"
    t.text "resume"
    t.date "fecha"
    t.string "author"
    t.boolean "borrador", default: true
    t.boolean "destacado"
    t.index ["post_category_id"], name: "index_posts_on_post_category_id"
    t.index ["qualification_id"], name: "index_posts_on_qualification_id"
  end

  create_table "provincias", force: :cascade do |t|
    t.string "nombre"
    t.bigint "region_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["region_id"], name: "index_provincias_on_region_id"
  end

  create_table "pueblos", force: :cascade do |t|
    t.string "nombre"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "qualifications", force: :cascade do |t|
    t.string "name"
    t.string "label"
    t.string "color"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "recurso_categories", force: :cascade do |t|
    t.string "name"
    t.string "description_short"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.string "icon_text"
    t.string "color"
    t.bigint "section_id", default: 1, null: false
    t.integer "order"
    t.integer "box_size", default: -1
    t.index ["section_id"], name: "index_recurso_categories_on_section_id"
  end

  create_table "recursos", force: :cascade do |t|
    t.string "url"
    t.string "description_short"
    t.string "description_long"
    t.string "reading_time"
    t.string "dificulty"
    t.boolean "recommended"
    t.text "resume"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.bigint "recurso_category_id"
    t.string "name"
    t.string "author"
    t.date "fecha"
    t.string "facebook"
    t.string "twitter"
    t.string "instagram"
    t.string "video"
    t.index ["recurso_category_id"], name: "index_recursos_on_recurso_category_id"
  end

  create_table "regions", force: :cascade do |t|
    t.string "nombre"
    t.integer "numero"
    t.string "num_romano"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "sections", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.integer "order"
  end

  create_table "settings", force: :cascade do |t|
    t.string "section"
    t.string "key"
    t.string "value"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "users", force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
  end

  add_foreign_key "active_storage_attachments", "active_storage_blobs", column: "blob_id"
  add_foreign_key "aportes", "aportantes"
  add_foreign_key "aportes", "candidatos"
  add_foreign_key "candidatos", "distritos"
  add_foreign_key "candidatos", "pactos"
  add_foreign_key "candidatos", "partidos"
  add_foreign_key "candidatos", "pueblos"
  add_foreign_key "comunas", "distritos"
  add_foreign_key "comunas", "provincias"
  add_foreign_key "distritos", "circunscripcions"
  add_foreign_key "faqs", "faq_categories"
  add_foreign_key "lugars", "comunas"
  add_foreign_key "members", "organizations"
  add_foreign_key "posts", "post_categories"
  add_foreign_key "posts", "qualifications"
  add_foreign_key "provincias", "regions"
  add_foreign_key "recurso_categories", "sections"
  add_foreign_key "recursos", "recurso_categories"
end
